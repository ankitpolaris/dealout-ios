//
//  BusinessDetailViewController.swift
//  DealOut
//
//  Created by ankit khanna on 4/21/16.
//  Copyright © 2016 Leonidas Orfanidis. All rights reserved.
//

import UIKit
import MapKit

class BusinessDetailViewController: UIViewController {
    
    
    @IBOutlet weak var businessName: UILabel!
    @IBOutlet weak var businessDescription: UILabel!
    @IBOutlet weak var businessProfileImageView: UIImageView!
    @IBOutlet weak var businessCoverImageView: UIImageView!
    @IBOutlet weak var businessStreetAddress: UILabel!
    @IBOutlet weak var mapView: MKMapView!
    
    var profile_image_url = ""
    var cover_image_url = ""
    var business_name = ""
    var business_description = ""
    var businss_street_address = ""
    var business_latitude = ""
    var business_longitude = ""
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.businessName.text = business_name
        self.businessDescription.text = business_description
        self.businessStreetAddress.text = businss_street_address
        self.businessProfileImageView.layer.borderColor = UIColor.whiteColor().CGColor
        self.businessProfileImageView.layer.borderWidth = 2
        let fileUrl = NSURL(string: profile_image_url as String)
        self.businessProfileImageView.hnk_setImageFromURL(fileUrl!)
        let fileUrl1 = NSURL(string: cover_image_url as String)
        self.businessCoverImageView.hnk_setImageFromURL(fileUrl1!)
        
        let location = CLLocationCoordinate2D(
//            latitude: 51.50007773,
//            longitude: -0.1246402
            latitude: Double(business_latitude)!,
            longitude: Double(business_longitude)!
        )
        
        let span = MKCoordinateSpanMake(0.05, 0.05)
        let region = MKCoordinateRegion(center: location, span: span)
        
        mapView.setRegion(region, animated: true)
        
        let annotation = MKPointAnnotation()
        annotation.coordinate = location
        annotation.title = "Business Map"
        annotation.subtitle = "Greece"
        
        mapView.addAnnotation(annotation)
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func close(sender: AnyObject) {
        
        self.dismissViewControllerAnimated(true, completion: {})
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
