//
//  FirstViewController.swift
//  DealOut
//
//  Created by ankit khanna on 2/10/16.
//  Copyright © 2016 Leonidas Orfanidis. All rights reserved.
//

import UIKit
import Foundation
import CoreLocation

extension String {
    func trunc(length: Int, trailing: String? = "...") -> String {
        if self.characters.count > length {
            return self.substringToIndex(self.startIndex.advancedBy(length)) + (trailing ?? "")
        } else {
            return self
        }
    }
}

class FirstViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource, UISearchResultsUpdating, CLLocationManagerDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    lazy var refreshControl = UIRefreshControl()
    
    let locationManager = CLLocationManager()
    let userGPSLat = NSUserDefaults.standardUserDefaults()
    let userGPSLong = NSUserDefaults.standardUserDefaults()
    
    
    var TableData:Array< datastruct > = Array < datastruct >()
    
    var filteredTableData :[(dealName:String , dealDescription: String, imageURL: String, dealCouponStatus: Int, dealCouponCode: String, dealID: String, consumed_by_username: String)] = []
    var brandAndBrandDescription1: [(dealName:String , dealDescription: String, imageURL: String, dealCouponStatus: Int, dealCouponCode: String, dealID: String, consumed_by_username: String)] = []
    
    var resultSearchController = UISearchController()
    
    enum ErrorHandler:ErrorType
    {
        case ErrorFetchingResults
    }
    
    struct datastruct
    {
        var imageurl:String?
        var image:UIImage? = nil
        var deal_id:String?
        var deal_name:String?
        var consume_flag:Int?
        var deal_description:String?
        var expiry_date:String?
        var deal_gps_latitude:String?
        var deal_gps_longitude:String?
        var deal_gps_distance:String?
        var kind_of_business:String?
        var deal_coupon_code:String?
        var consumed_by_username:String?
        var count_deals:String?
        
        
        init(add: NSDictionary)
        {
            imageurl = add["deal_image"] as? String
            deal_id = add["deal_id"] as? String
            deal_name = add["deal_name"] as? String
            consume_flag = add["consume_flag"] as? Int
            deal_description = add["deal_description"] as? String
            expiry_date = add["expiry_date"] as? String
            deal_gps_latitude = add["deal_gps_latitude"] as? String
            deal_gps_longitude = add["deal_gps_longitude"] as? String
            deal_gps_distance = add["deal_gps_distance"] as? String
            kind_of_business = add["kind_of_business"] as? String
            deal_coupon_code = add["deal_coupon_code"] as? String
            consumed_by_username = add["consumed_by_username"] as? String
            count_deals = add["count_deals"] as? String
            
        }
    }
    deinit{
        if let superView = resultSearchController.view.superview
        {
            superView.removeFromSuperview()
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        // Left menu panel code
        addSlideMenuButton()
        // End of left menu panel code
        
        fetchGPSCoordinates()
        tableView.delegate = self
        tableView.dataSource = self
        self.activityIndicator.hidden = true
        self.refreshControl = UIRefreshControl()
        self.refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        self.refreshControl.addTarget(self, action: "refresh:", forControlEvents: UIControlEvents.ValueChanged)
        tableView.addSubview(refreshControl)
        definesPresentationContext = true
        fetchAllDeals()
        
        if #available(iOS 9.0, *) {
            self.resultSearchController.loadViewIfNeeded()// iOS 9
        } else {
            // Fallback on earlier versions
            let _ = self.resultSearchController.view          // iOS 8
        }
        
        self.resultSearchController = ({
            let controller = UISearchController(searchResultsController: nil)
            controller.searchResultsUpdater = self
            controller.dimsBackgroundDuringPresentation = false
            controller.searchBar.sizeToFit()
            self.tableView.tableHeaderView = controller.searchBar
            
            return controller
        })()
        
        // Reload the table
        self.tableView.reloadData()
        
        
    }
    override func viewWillAppear(animated: Bool) {
        let logo = UIImage(named: "dealout_ios_logo") as UIImage?
        let imageView = UIImageView(image:logo)
        
        imageView.frame.size.width = 60;
        imageView.frame.size.height = 25;
        imageView.contentMode = UIViewContentMode.Center
        
        self.navigationItem.titleView = imageView
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (self.resultSearchController.active) {
            return self.filteredTableData.count
        }
        else {
        //    print ("Table Data : \(TableData.count)")
            return TableData.count
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let dealStatusMesssageTaken = NSLocalizedString("USER_MESSAGE20", comment: "TAKEN")
        let dealStatusMesssageAvailable = NSLocalizedString("USER_MESSAGE21", comment: "AVAILABLE")
        
        let countDeals = TableData[indexPath.row].count_deals
     
        if (countDeals!=="NO_DEALS") {
            let cell: NoDealsTableViewCell = tableView.dequeueReusableCellWithIdentifier("nodeals", forIndexPath: indexPath) as! NoDealsTableViewCell
            return cell
            
        }
            
        else {
            let cell: AllDealsTableViewCell = tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath) as! AllDealsTableViewCell
            cell.couponStatus.transform = CGAffineTransformMakeRotation(CGFloat(M_PI_2));
            if (self.resultSearchController.active) {
                

                
                cell.dealName.text = filteredTableData[indexPath.row].dealName.trunc(70)
                cell.dealDescription.text = filteredTableData[indexPath.row].dealDescription.trunc(50)
                let consumeFlag = filteredTableData[indexPath.row].dealCouponStatus
                if (consumeFlag==0) {
                    let lightBlueColor = UIColor(red: 70.0/255.0, green: 192.0/255.0, blue: 201.0/255.0, alpha: 1.0)
                    
                    cell.couponStatus.text = dealStatusMesssageAvailable
                    cell.dealCouponStatusView.backgroundColor = lightBlueColor
                }
                else if (consumeFlag==1) {
                    cell.couponStatus.text = dealStatusMesssageTaken
                    cell.dealCouponStatusView.backgroundColor = UIColor.redColor()
                    
                }
                let imageURL = filteredTableData[indexPath.row].imageURL
                let fileUrl = NSURL(string: imageURL as String)
                
                cell.dealsImageView.layer.cornerRadius = 40
                cell.dealsImageView.hnk_setImageFromURL(fileUrl!)
                return cell
                
            }
            else {
                cell.dealName.text = TableData[indexPath.row].deal_name!.trunc(70)
                let dealDescription = TableData[indexPath.row].deal_description!.trunc(50)
                cell.dealDescription.text = dealDescription
                let consumeFlag = TableData[indexPath.row].consume_flag
                if (consumeFlag==0) {
                    let lightBlueColor = UIColor(red: 70.0/255.0, green: 192.0/255.0, blue: 201.0/255.0, alpha: 1.0)
                    
                    cell.couponStatus.text = dealStatusMesssageAvailable
                    cell.dealCouponStatusView.backgroundColor = lightBlueColor
                }
                else if (consumeFlag==1) {
                    cell.couponStatus.text = dealStatusMesssageTaken
                    cell.dealCouponStatusView.backgroundColor = UIColor.redColor()
                    
                }
                let imageURL = TableData[indexPath.row].imageurl
                let fileUrl = NSURL(string: imageURL! as String)
                
                cell.dealsImageView.layer.cornerRadius = 40
                cell.dealsImageView.hnk_setImageFromURL(fileUrl!)
                return cell
            }
        }
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 120.0
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let username = NSUserDefaults.standardUserDefaults()
        let loginUsername = username.stringForKey("loginUsername")
        let dateWhenUserTookDeal = NSUserDefaults.standardUserDefaults()
        let dateWhenDealTakenByUser = dateWhenUserTookDeal.stringForKey("dateWhenDealTakenByUser")
        
        let todaysDate:NSDate = NSDate()
        let dateFormatter:NSDateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let todayString:String = dateFormatter.stringFromDate(todaysDate)
        print ("Todays date: \(todayString)")
        
        //   print("Consumed by user: \(TableData[indexPath.row].consumed_by_username)")
        if (self.resultSearchController.active)  {
            
            if (filteredTableData[indexPath.row].dealCouponStatus == 1 && filteredTableData[indexPath.row].consumed_by_username != loginUsername){
                
                // alert box code below
                let sorry = NSLocalizedString("USER_MESSAGE1", comment: "Sorry")
               let dealTaken = NSLocalizedString("USER_MESSAGE2", comment: "This deal is already taken by other user.")
                let alert = UIAlertController(title: sorry, message:dealTaken, preferredStyle: .Alert)
                let action = UIAlertAction(title: NSLocalizedString("USER_MESSAGE3", comment: "OK"), style: .Default) { _ in
                    // Put here any code that you would like to execute when
                    // the user taps that OK button (may be empty in your case if that's just
                    // an informative alert
                    self.dismissViewControllerAnimated(true, completion: {})
                }
                alert.addAction(action)
                self.presentViewController(alert, animated: true, completion: nil)
                // alert box code end
                
            }
            else if (filteredTableData[indexPath.row].dealCouponStatus == 1 && filteredTableData[indexPath.row].consumed_by_username == loginUsername) {
                
                self.performSegueWithIdentifier("scratch", sender: indexPath)
            }
            else {
                
                if (dateWhenDealTakenByUser == todayString) {
                    
                    // alert box code below
                    let scratchDeal = NSLocalizedString("USER_MESSAGE4", comment: "Scratch Deal")
                    let dealMessage = NSLocalizedString("USER_MESSAGE5", comment: "You can scratch only 1 deal per day from this list. Check My Deals for more deals or come back tomorrow to view more deals here.")
                    let OK = NSLocalizedString("USER_MESSAGE3", comment: "OK")
                    
                    let alert = UIAlertController(title: scratchDeal, message:dealMessage, preferredStyle: .Alert)
                    let action = UIAlertAction(title: OK, style: .Default) { _ in
                        // Put here any code that you would like to execute when
                        // the user taps that OK button (may be empty in your case if that's just
                        // an informative alert
                        
                    }
                    alert.addAction(action)
                    self.presentViewController(alert, animated: true, completion: nil)
                    // alert box code end
                }
                else {
                    // alert box code below
                    let scratchDeal = NSLocalizedString("USER_MESSAGE4", comment: "Scratch Deal")
                    let dealMessage = NSLocalizedString("USER_MESSAGE8", comment: "After scraching this deal, you will not be able to scratch any more deals for today. Also, you will not be allowed to take any further deal from this business for next 5 day. Do you wish to proceed ?")
                    let YES = NSLocalizedString("USER_MESSAGE6", comment: "Yes")
                    let NO = NSLocalizedString("USER_MESSAGE7", comment: "No")
                    
                    let alert = UIAlertController(title: scratchDeal, message:dealMessage, preferredStyle: .Alert)
                    let action = UIAlertAction(title: YES, style: .Default) { _ in
                        // Put here any code that you would like to execute when
                        // the user taps that OK button (may be empty in your case if that's just
                        // an informative alert
                        self.performSegueWithIdentifier("scratch", sender: indexPath)
                    }
                    let action1 = UIAlertAction(title: NO, style: .Default) { _ in
                        // Put here any code that you would like to execute when
                    }
                    alert.addAction(action)
                    alert.addAction(action1)
                    
                    self.presentViewController(alert, animated: true, completion: nil)
                    // alert box code end
                    
                }
            }
        }
            
            
        else {
            
            
            if (TableData[indexPath.row].consume_flag == 1 && TableData[indexPath.row].consumed_by_username != loginUsername){
                
                // alert box code below
                let sorry = NSLocalizedString("USER_MESSAGE1", comment: "Sorry")
                let dealTaken = NSLocalizedString("USER_MESSAGE2", comment: "This deal is already taken by other user.")
                let alert = UIAlertController(title: sorry, message:dealTaken, preferredStyle: .Alert)
                let action = UIAlertAction(title: NSLocalizedString("USER_MESSAGE3", comment: "OK"), style: .Default) { _ in
                    // Put here any code that you would like to execute when
                    // the user taps that OK button (may be empty in your case if that's just
                    // an informative alert
                    self.dismissViewControllerAnimated(true, completion: {})
                }
                alert.addAction(action)
                self.presentViewController(alert, animated: true, completion: nil)
                // alert box code end
                
            }
            else if (TableData[indexPath.row].consume_flag == 1 && TableData[indexPath.row].consumed_by_username == loginUsername) {
                
                self.performSegueWithIdentifier("scratch", sender: indexPath)
            }
            else {
                if (dateWhenDealTakenByUser == todayString) {
                    
                    // alert box code below
                    
                    let scratchDeal = NSLocalizedString("USER_MESSAGE4", comment: "Scratch Deal")
                    let dealMessage = NSLocalizedString("USER_MESSAGE5", comment: "You can scratch only 1 deal per day from this list. Check My Deals for more deals or come back tomorrow to view more deals here.")
                    let OK = NSLocalizedString("USER_MESSAGE3", comment: "OK")
                    
                    let alert = UIAlertController(title: scratchDeal, message:dealMessage, preferredStyle: .Alert)
                    let action = UIAlertAction(title: OK, style: .Default) { _ in
                        // Put here any code that you would like to execute when
                        // the user taps that OK button (may be empty in your case if that's just
                        // an informative alert
                        
                    }
                    alert.addAction(action)
                    self.presentViewController(alert, animated: true, completion: nil)
                    // alert box code end
                }
                else {
                    // alert box code below
                    let scratchDeal = NSLocalizedString("USER_MESSAGE4", comment: "Scratch Deal")
                    let dealMessage = NSLocalizedString("USER_MESSAGE8", comment: "After scraching this deal, you will not be able to scratch any more deals for today. Also, you will not be allowed to take any further deal from this business for next 5 day. Do you wish to proceed ?")
                    let YES = NSLocalizedString("USER_MESSAGE6", comment: "Yes")
                    let NO = NSLocalizedString("USER_MESSAGE7", comment: "No")
                    
                    let alert = UIAlertController(title: scratchDeal, message:dealMessage, preferredStyle: .Alert)
                    let action = UIAlertAction(title: YES, style: .Default) { _ in
                        // Put here any code that you would like to execute when
                        // the user taps that OK button (may be empty in your case if that's just
                        // an informative alert
                        self.performSegueWithIdentifier("scratch", sender: indexPath)
                    }
                    let action1 = UIAlertAction(title: NO, style: .Default) { _ in
                        // Put here any code that you would like to execute when
                    }
                    alert.addAction(action)
                    alert.addAction(action1)
                    
                    self.presentViewController(alert, animated: true, completion: nil)
                    // alert box code end
                    
                }
            }
        }
    }
    
    
    func updateSearchResultsForSearchController(searchController: UISearchController) {
        guard let searchString = searchController.searchBar.text else {
            return
        }
        
        filteredTableData = brandAndBrandDescription1.filter({ (country) -> Bool in
            let countryText:NSString = country.dealName
            
            return (countryText.rangeOfString(searchString, options: NSStringCompareOptions.CaseInsensitiveSearch).location) != NSNotFound
        })
        
        // Reload the tableview.
        self.tableView.reloadData()
    }
    
    
    
    func loadDataArray() {
        
        for i in 0...TableData.count-1 {
            //                        print ("I:\(i)")
            //                        print ("Table Count:\(TableData.count)")
            let DealID = TableData[i].deal_id
            let DealName = TableData[i].deal_name
            let DealDescription = TableData[i].deal_description
            let BusinessImageURL = TableData[i].imageurl
            let DealCouponStatus = TableData[i].consume_flag
            let DealCouponCode = TableData[i].deal_coupon_code
            let ConsumedByUsername = TableData[i].consumed_by_username
            
            
            brandAndBrandDescription1.append(dealName: DealName! , dealDescription: DealDescription!, imageURL: BusinessImageURL!, dealCouponStatus: DealCouponStatus!, dealCouponCode: DealCouponCode!, dealID: DealID!, consumed_by_username: ConsumedByUsername!)
            
        }
    }
    func fetchAllDeals () {
        let authstate = CLLocationManager.authorizationStatus()
        let latitude = userGPSLat.stringForKey("userLatitude")!
        let longitude = userGPSLong.stringForKey("userLongitude")!
        
        if (authstate == CLAuthorizationStatus.AuthorizedWhenInUse && longitude != "NO_LOCATION") {
            
            print ("Location is authorized")
            
          //  if Reachability.isConnectedToNetwork() == true {
            let status = checkNetworkStatus()
            
            if status == true {
                self.activityIndicator.hidden = false
                self.activityIndicator.startAnimating()
                
                let username = NSUserDefaults.standardUserDefaults()
                let loginUsername = username.stringForKey("loginUsername")
                
                var urlAsString = api_url+"alldeals_ios_gps.php"
                urlAsString = urlAsString+"?apiKey="+apiKey+"&cust_username="+loginUsername!+"&gps=1"+"&latitude="+latitude+"&longitude="+longitude
                print (urlAsString)
                
                let url = NSURL(string: urlAsString)!
                let session = NSURLSession.sharedSession()
                
                let request = NSMutableURLRequest(URL: url)
                request.HTTPMethod = "GET"
                request.cachePolicy = NSURLRequestCachePolicy.ReloadIgnoringCacheData
                
                let task = session.dataTaskWithRequest(request) {
                    (
                    let data, let response, let error) in
                    
                    guard let _:NSData = data, let _:NSURLResponse = response  where error == nil else {
                        print("error")
                        return
                    }
                    self.activityIndicator.hidden = false
                    self.activityIndicator.startAnimating()
                    dispatch_async(dispatch_get_main_queue(), {
                        let json = NSString(data: data!, encoding: NSASCIIStringEncoding)
                        self.extract_json(json!)
                        self.activityIndicator.stopAnimating()
                        self.activityIndicator.hidden = true
                        return
                    })
                    
                }
                
                task.resume()
                
                
            }
            else {
                
                print("Internet connection FAILED")
                // alert box code below
                let title = NSLocalizedString("USER_MESSAGE9", comment: "Not Connected")
                let Message = NSLocalizedString("USER_MESSAGE10", comment: "You are not connected to internet!.")
                let OK = NSLocalizedString("USER_MESSAGE3", comment: "OK")
                
                let alert = UIAlertController(title: title, message:Message, preferredStyle: .Alert)
                let action = UIAlertAction(title: OK, style: .Default) { _ in
                    // Put here any code that you would like to execute when
                    // the user taps that OK button (may be empty in your case if that's just
                    // an informative alert
                    self.dismissViewControllerAnimated(true, completion: {})
                }
                alert.addAction(action)
                self.presentViewController(alert, animated: true, completion: nil)
                // alert box code end
                
            }
            
        }
        else {
          //  if Reachability.isConnectedToNetwork() == true {
            let status = checkNetworkStatus()
            
            if status == true {
                self.activityIndicator.hidden = false
                self.activityIndicator.startAnimating()
                
                let username = NSUserDefaults.standardUserDefaults()
                let loginUsername = username.stringForKey("loginUsername")
                
                var urlAsString = api_url+"alldeals_ios.php"
                urlAsString = urlAsString+"?apiKey="+apiKey+"&cust_username="+loginUsername!
                print (urlAsString)
                
                let url = NSURL(string: urlAsString)!
                let session = NSURLSession.sharedSession()
                
                let request = NSMutableURLRequest(URL: url)
                request.HTTPMethod = "GET"
                request.cachePolicy = NSURLRequestCachePolicy.ReloadIgnoringCacheData
                
                let task = session.dataTaskWithRequest(request) {
                    (
                    let data, let response, let error) in
                    
                    guard let _:NSData = data, let _:NSURLResponse = response  where error == nil else {
                        print("error")
                        return
                    }
                    self.activityIndicator.hidden = false
                    self.activityIndicator.startAnimating()
                    dispatch_async(dispatch_get_main_queue(), {
                        let json = NSString(data: data!, encoding: NSASCIIStringEncoding)
                        self.extract_json(json!)
                        self.activityIndicator.stopAnimating()
                        self.activityIndicator.hidden = true
                        return
                    })
                    
                }
                
                task.resume()
                
                
            }
            else {
                
                print("Internet connection FAILED")
                // alert box code below
                let title = NSLocalizedString("USER_MESSAGE9", comment: "Not Connected")
                let Message = NSLocalizedString("USER_MESSAGE10", comment: "You are not connected to internet!.")
                let OK = NSLocalizedString("USER_MESSAGE3", comment: "OK")
                
                
                let alert = UIAlertController(title: title, message:Message, preferredStyle: .Alert)
                let action = UIAlertAction(title: OK, style: .Default) { _ in
                    // Put here any code that you would like to execute when
                    // the user taps that OK button (may be empty in your case if that's just
                    // an informative alert
                    self.dismissViewControllerAnimated(true, completion: {})
                }
                alert.addAction(action)
                self.presentViewController(alert, animated: true, completion: nil)
                // alert box code end
                
            }
            
        }
        
    }
    
    
    func extract_json(data:NSString)
        
    {
        var parseError: NSError?
        let jsonData:NSData = data.dataUsingEncoding(NSASCIIStringEncoding)!
        let json: AnyObject?
        do {
            json = try NSJSONSerialization.JSONObjectWithData(jsonData, options: [])
        } catch let error as NSError {
            parseError = error
            json = nil
        }
        if (parseError == nil)
        {
            if let list:NSArray = json as? NSArray
            {
                //                print ("Inside if let")
                //                print (list)
                //                                print ("List count\(list.count)")
                
                for (var i = 0; i < list.count; i += 1 )
                {
                    if let data_block = list[i] as? NSDictionary
                    {
                        TableData.append(datastruct(add: data_block))
                    }
                }
                do_table_refresh()
            }
            
        }
        
        
    }
    func refresh(sender:AnyObject) {
       let status = checkNetworkStatus()
        
        if status == true {
        //if Reachability.isConnectedToNetwork() == true {
        self.TableData.removeAll()
        fetchAllDeals()
        }
         else {
            print("Internet connection FAILED")
            // alert box code below
            let title = NSLocalizedString("USER_MESSAGE9", comment: "Not Connected")
            let Message = NSLocalizedString("USER_MESSAGE10", comment: "You are not connected to internet!.")
            let OK = NSLocalizedString("USER_MESSAGE3", comment: "OK")
            
            let alert = UIAlertController(title: title, message:Message, preferredStyle: .Alert)
            let action = UIAlertAction(title: OK, style: .Default) { _ in
                // Put here any code that you would like to execute when
                // the user taps that OK button (may be empty in your case if that's just
                // an informative alert
                self.refreshControl.endRefreshing()
                self.dismissViewControllerAnimated(true, completion: {})
            }
            alert.addAction(action)
            self.presentViewController(alert, animated: true, completion: nil)
            // alert box code end
        }
        
    }
    func do_table_refresh()
    {
        dispatch_async(dispatch_get_main_queue(), {
            self.tableView.reloadData()
            self.refreshControl.endRefreshing()
            if (self.TableData.count>1) {
            self.loadDataArray()
            }
            return
        })
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        
        if segue.identifier == "scratch" {
            
            if (self.resultSearchController.active) {
                
                let indexPath = self.tableView.indexPathForSelectedRow
                if indexPath != nil {
                    let dealID = filteredTableData[indexPath!.row].dealID
                    let dealName = filteredTableData[indexPath!.row].dealName
                    let dealDescription = filteredTableData[indexPath!.row].dealDescription
                    let dealCouponCode = filteredTableData[indexPath!.row].dealCouponCode
                    let dealImageURL = filteredTableData[indexPath!.row].imageURL
                    let dealCouponStatus = filteredTableData[indexPath!.row].dealCouponStatus
                    let comingFrom = "ALL_DEALS"
                    let destinationVC = segue.destinationViewController as! ScratchViewController
                    
                    destinationVC.dealID = dealID as String
                    destinationVC.dealName = dealName as String
                    
                    destinationVC.dealDescription = dealDescription as String
                    destinationVC.dealCouponCode = dealCouponCode as String
                    destinationVC.imageurl = dealImageURL as String
                    destinationVC.dealCouponStatus = dealCouponStatus as Int
                    destinationVC.coming_from = comingFrom as String
                }
                
                
            }
            else {
                
                let indexPath: NSIndexPath = sender as! NSIndexPath
                let comingFrom = "ALL_DEALS"
                let dealID = TableData[indexPath.row].deal_id
                let dealName = TableData[indexPath.row].deal_name
                let dealDescription = TableData[indexPath.row].deal_description
                let dealCouponCode = TableData[indexPath.row].deal_coupon_code
                let dealImageURL = TableData[indexPath.row].imageurl
                let dealCouponStatus = TableData[indexPath.row].consume_flag
                let destinationVC = segue.destinationViewController as! ScratchViewController
                
                destinationVC.dealID = dealID! as String
                destinationVC.dealName = dealName! as String
                destinationVC.dealDescription = dealDescription! as String
                destinationVC.dealCouponCode = dealCouponCode! as String
                destinationVC.imageurl = dealImageURL! as String
                destinationVC.dealCouponStatus = dealCouponStatus! as Int
                destinationVC.coming_from = comingFrom as String
                
                
            }
        }
        
        
    }
    func fetchGPSCoordinates() {
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.requestWhenInUseAuthorization()
            if #available(iOS 9.0, *) {
                locationManager.requestLocation()
            } else {
                // Fallback on earlier versions
            }
            
        }
        
    }
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        CLGeocoder().reverseGeocodeLocation(manager.location!, completionHandler: {(placemarks, error)->Void in
            let userLocation:CLLocation = locations[0]
            let long = userLocation.coordinate.longitude;
            let lat = userLocation.coordinate.latitude;
            
            
            self.userGPSLat.setObject("\(lat)", forKey: "userLatitude")
            self.userGPSLong.setObject("\(long)", forKey: "userLongitude")
            print (lat,long)
            if (error != nil) {
                print("Reverse geocoder failed with error" + error!.localizedDescription)
                return
            }
            
        })
    }
    
    func locationManager(manager: CLLocationManager, didFailWithError error: NSError) {
        print("Error while updating location " + error.localizedDescription)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}

