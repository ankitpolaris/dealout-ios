//
//  LocalBusinessTableViewCell.swift
//  DealOut
//
//  Created by ankit khanna on 4/27/16.
//  Copyright © 2016 Leonidas Orfanidis. All rights reserved.
//

import UIKit

class LocalBusinessTableViewCell: UITableViewCell {

    @IBOutlet weak var couponStatus: UILabel!
    @IBOutlet weak var businessName: UILabel!
    @IBOutlet weak var businessDescription: UILabel!
    @IBOutlet weak var businessImageView: UIImageView!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
