//
//  WalkthroughPageViewController.swift
//  DealOut
//
//  Created by ankit khanna on 3/5/16.
//  Copyright © 2016 Leonidas Orfanidis. All rights reserved.
//

import UIKit
import CoreLocation

class WalkthroughPageViewController: UIPageViewController, UIPageViewControllerDataSource, CLLocationManagerDelegate {
    
    let userLatitude = NSUserDefaults.standardUserDefaults()
    let userLongitude = NSUserDefaults.standardUserDefaults()
    
       let locationManager = CLLocationManager()
 
//
//      var pageHeadings = ["All Deals", "Scratch Deals", "Scratch Deals", "All Deals", "Local Business"]
    var pageHeadings = ["Όλες οι Προσφορές", "Γρατσουνιά Συμφωνίες", "Γρατσουνιά Συμφωνίες", "Όλες οι Προσφορές", "Τοπική επιχείρηση"]
    
    var pageImages = ["walk1.png", "walk2.png", "walk3.png", "walk4.png", "walk5.png"]
    
//    var pageContent = ["All Deals are created every day and you can take one deal each day.", "My Deals are sent to users by admin. Scratch the deal to unlock deal coupon code.", "Scratch on the scratch card and you'll see the code.", "Deals showing in red are the one's taken by other users. All deals are valid for 1 day.", "Local Business are shown based on your location and distance from business."]
        var pageContent = ["Όλες οι προσφορές που δημιουργούνται κάθε μέρα και μπορείτε να πάρετε μια διαπραγμάτευση κάθε μέρα.", "Οι προσφορές μου αποστέλλονται σε χρήστες από το admin. Ξύστε τη συμφωνία για να ξεκλειδώσετε συμφωνία κωδικό κουπονιού.", "Γρατσουνιά στο ξυστό και θα δείτε τον κώδικα. "," Προσφορές δείχνει με κόκκινο είναι αυτά που λαμβάνονται από άλλους χρήστες. Όλες οι προσφορές ισχύουν για 1 ημέρα.", "Τοπικών επιχειρήσεων εμφανίζονται με βάση τη θέση και την απόσταση σας από τις επιχειρήσεις."]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        fetchGPSCoordinates()
        self.userLatitude.setObject("NO_LOCATION", forKey: "userLatitude")
        self.userLongitude.setObject("NO_LOCATION", forKey: "userLongitude")
         NSUserDefaults.standardUserDefaults().synchronize()
        dataSource = self
        
        if let startingViewController = viewControllerAtIndex(0) {
            setViewControllers([startingViewController], direction: .Forward,animated: true, completion: nil)
        }
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func pageViewController(pageViewController: UIPageViewController, viewControllerAfterViewController viewController: UIViewController) -> UIViewController? {
            var index = (viewController as! WalkthroughContentViewController).index
            index += 1
            return viewControllerAtIndex(index)
    }
    
    func pageViewController(pageViewController: UIPageViewController, viewControllerBeforeViewController viewController: UIViewController) -> UIViewController? {
                var index = (viewController as! WalkthroughContentViewController).index
                index -= 1
                return viewControllerAtIndex(index)
    }
    
    func viewControllerAtIndex(index: Int) -> WalkthroughContentViewController? {
        if index == NSNotFound || index < 0 || index >= pageHeadings.count {
        return nil
        }
        // Create a new view controller and pass suitable data.
        if let pageContentViewController =
        storyboard?.instantiateViewControllerWithIdentifier("WalkthroughContentViewController")
        as? WalkthroughContentViewController {
        
        pageContentViewController.imageFile = pageImages[index]
        pageContentViewController.heading = pageHeadings[index]
        pageContentViewController.content = pageContent[index]
        pageContentViewController.index = index
        return pageContentViewController
        }
        return nil
    }
    
    func forward(index:Int) {
                    
                    if let nextViewController = viewControllerAtIndex(index+1) {
        setViewControllers([nextViewController], direction: .Forward, animated:true, completion:nil)
                    }
    }
    
    func locationManager(manager: CLLocationManager, didFailWithError error: NSError) {
        print("Error while updating location " + error.localizedDescription)
    }
    
    
    func fetchGPSCoordinates() {
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.requestWhenInUseAuthorization()
            if #available(iOS 9.0, *) {
                locationManager.requestLocation()
            } else {
                // Fallback on earlier versions
            }
            
        }
        
    }
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        CLGeocoder().reverseGeocodeLocation(manager.location!, completionHandler: {(placemarks, error)->Void in
            let userLocation:CLLocation = locations[0]
            let long = userLocation.coordinate.longitude;
            let lat = userLocation.coordinate.latitude;
            //            self.latitude = String(lat)
            //            self.longitude = String(long)
            self.userLatitude.setObject("\(lat)", forKey: "userLatitude")
            self.userLongitude.setObject("\(long)", forKey: "userLongitude")
            NSUserDefaults.standardUserDefaults().synchronize()
            //  print (lat,long)
            if (error != nil) {
                print("Reverse geocoder failed with error" + error!.localizedDescription)
                return
            }
 
        })
    }
    
    
//    func presentationCountForPageViewController(pageViewController: UIPageViewController) -> Int {
//                    return pageImages.count
//    }
//    
//    func presentationIndexForPageViewController(pageViewController: UIPageViewController) -> Int {
//        if let pageContentViewController =
//            storyboard?.instantiateViewControllerWithIdentifier("WalkthroughContentViewController")
//            as? WalkthroughContentViewController {
//            return pageContentViewController.index
//        }
//        return 0
//    }
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
    
}
