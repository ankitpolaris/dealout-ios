//
//  LocalBusinessViewController.swift
//  DealOut
//
//  Created by ankit khanna on 4/21/16.
//  Copyright © 2016 Leonidas Orfanidis. All rights reserved.
//

import UIKit
import CoreLocation

class LocalBusinessViewController: BaseViewController, UITableViewDataSource, UITableViewDelegate,UISearchResultsUpdating, CLLocationManagerDelegate {
    
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    lazy var refreshControl = UIRefreshControl()
    
    let locationManager = CLLocationManager()
    let userGPSLat = NSUserDefaults.standardUserDefaults()
    let userGPSLong = NSUserDefaults.standardUserDefaults()
 
    
    var TableData:Array< datastruct > = Array < datastruct >()
    
    var filteredTableData :[(brandName:String , brandDescription: String, imageURL: String, advertisementDistance: String, businessStreeAddress: String, profile_cover_image_url: String, business_latitude: String, business_longitude: String)] = []
    var brandAndBrandDescription: [(brandName:String , brandDescription: String, imageURL: String, advertisementDistance: String, businessStreeAddress: String, profile_cover_image_url: String, business_latitude: String, business_longitude: String)] = []
    
    
    var resultSearchController = UISearchController()
    
    enum ErrorHandler:ErrorType
    {
        case ErrorFetchingResults
    }
    
    struct datastruct
    {
        var imageurl:String?
        var image:UIImage? = nil
        var advertisement_id:String?
        var advertiser_username:String?
        var advertisement_brand:String?
        var advertisement_description:String?
        var advertisement_url:String?
        var advertisement_gps_latitude:String?
        var advertisement_gps_longitude:String?
        var advertisement_distance:String?
        var advertisement_priority:String?
        var advertisement_street:String?
        var advertiser_fb_url:String?
        var profile_cover_image_url:String?
        var count_business:String?
        
        
        init(add: NSDictionary)
        {
            imageurl = add["advertisement_image"] as? String
            advertisement_id = add["advertisement_id"] as? String
            advertiser_username = add["advertiser_username"] as? String
            advertisement_brand = add["advertisement_brand"] as? String
            advertisement_description = add["advertisement_description"] as? String
            advertisement_url = add["advertisement_url"] as? String
            advertisement_gps_latitude = add["advertisement_gps_latitude"] as? String
            advertisement_gps_longitude = add["advertisement_gps_longitude"] as? String
            advertisement_distance = add["advertisement_distance"] as? String
            advertisement_priority = add["advertisement_priority"] as? String
            advertisement_street = add["advertisement_street"] as? String
            advertiser_fb_url = add["advertiser_fb_url"] as? String
            profile_cover_image_url = add["profile_cover_image_url"] as? String
            count_business = add["count_business"] as? String
            
            
        }
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        fetchGPSCoordinates()
    
        fetchLocalBusiness()
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
    //    self.activityIndicator.hidden = true
        self.refreshControl = UIRefreshControl()
        self.refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        self.refreshControl.addTarget(self, action: #selector(LocalBusinessViewController.refresh(_:)), forControlEvents: UIControlEvents.ValueChanged)
        tableView.addSubview(refreshControl)
        addSlideMenuButton()
        
        // Do any additional setup after loading the view.
        if #available(iOS 9.0, *) {
            self.resultSearchController.loadViewIfNeeded()// iOS 9
        } else {
            // Fallback on earlier versions
            let _ = self.resultSearchController.view          // iOS 8
        }
        
        self.resultSearchController = ({
            let controller = UISearchController(searchResultsController: nil)
            controller.searchResultsUpdater = self
            controller.dimsBackgroundDuringPresentation = false
            controller.searchBar.sizeToFit()
            self.tableView.tableHeaderView = controller.searchBar
            
            return controller
        })()
        // Reload the table
        self.tableView.reloadData()
        
        
    }
    
    override func viewWillAppear(animated: Bool) {
        let logo = UIImage(named: "dealout_ios_logo") as UIImage?
        let imageView = UIImageView(image:logo)
        
        imageView.frame.size.width = 60;
        imageView.frame.size.height = 25;
        imageView.contentMode = UIViewContentMode.Center
        
        self.navigationItem.titleView = imageView
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (self.resultSearchController.active) {
            return self.filteredTableData.count
        }
        else {
            return TableData.count
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let countBusiness = TableData[indexPath.row].count_business
         print ("CountBusiness : \(countBusiness!)")
        if (countBusiness!=="NO_BUSINESS") {
            let cell: NoBusinessTableViewCell = tableView.dequeueReusableCellWithIdentifier("nobusiness", forIndexPath: indexPath) as! NoBusinessTableViewCell
            return cell
            
        }
        else {
        let cell: LocalBusinessTableViewCell = tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath) as! LocalBusinessTableViewCell
        cell.couponStatus.transform = CGAffineTransformMakeRotation(CGFloat(M_PI_2));
        if (self.resultSearchController.active) {
            cell.businessName.text = filteredTableData[indexPath.row].brandName
            cell.businessDescription.text = filteredTableData[indexPath.row].brandDescription
            cell.couponStatus.text = filteredTableData[indexPath.row].advertisementDistance+"mtr"
            let imageURL = filteredTableData[indexPath.row].imageURL
            let fileUrl = NSURL(string: imageURL as String)
            cell.businessImageView.layer.cornerRadius = 40
            cell.businessImageView.hnk_setImageFromURL(fileUrl!)
            return cell
        }
        else {
            cell.businessName.text = TableData[indexPath.row].advertisement_brand
            cell.businessDescription.text = TableData[indexPath.row].advertisement_description
            cell.couponStatus.text = TableData[indexPath.row].advertisement_distance!+"mtr"
            let imageURL = TableData[indexPath.row].imageurl
            let fileUrl = NSURL(string: imageURL! as String)
            cell.businessImageView.layer.cornerRadius = 40
            cell.businessImageView.hnk_setImageFromURL(fileUrl!)
            return cell
        }
        }
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 120.0
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        self.performSegueWithIdentifier("businessdetail", sender: indexPath)
        
    }
    
    
    func updateSearchResultsForSearchController(searchController: UISearchController) {
        guard let searchString = searchController.searchBar.text else {
            return
        }
        
        filteredTableData = brandAndBrandDescription.filter({ (country) -> Bool in
            let countryText:NSString = country.brandName
            
            return (countryText.rangeOfString(searchString, options: NSStringCompareOptions.CaseInsensitiveSearch).location) != NSNotFound
        })
        
        // Reload the tableview.
        self.tableView.reloadData()
    }
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        CLGeocoder().reverseGeocodeLocation(manager.location!, completionHandler: {(placemarks, error)->Void in
            let userLocation:CLLocation = locations[0]
            let long = userLocation.coordinate.longitude;
            let lat = userLocation.coordinate.latitude;

            
            self.userGPSLat.setObject("\(lat)", forKey: "userLatitude")
            self.userGPSLong.setObject("\(long)", forKey: "userLongitude")
            print (lat,long)
            if (error != nil) {
                print("Reverse geocoder failed with error" + error!.localizedDescription)
                return
            }

        })
    }
    
    func locationManager(manager: CLLocationManager, didFailWithError error: NSError) {
        print("Error while updating location " + error.localizedDescription)
    }
    func loadDataArray() {
        
        for i in 0...TableData.count-1 {
            //            print ("I:\(i)")
            //            print ("Table Count:\(TableData.count)")
            
            let advertiserBrand = TableData[i].advertisement_brand
            let advertiserBrandDescription = TableData[i].advertisement_description
            let advertisementDistance = TableData[i].advertisement_distance
            let businessImageURL = TableData[i].imageurl
            let businessStreetAddress = TableData[i].advertisement_street
            let profile_cover_image_url = TableData[i].profile_cover_image_url
            let business_latitude = TableData[i].advertisement_gps_latitude
            let business_longitude = TableData[i].advertisement_gps_longitude
            
            
            brandAndBrandDescription.append(brandName:advertiserBrand! , brandDescription:advertiserBrandDescription!, imageURL: businessImageURL!, advertisementDistance: advertisementDistance!, businessStreeAddress: businessStreetAddress!, profile_cover_image_url: profile_cover_image_url!, business_latitude: business_latitude!, business_longitude: business_longitude!)
            
        }
    }
    
    
    func fetchLocalBusiness() {
        let authstate = CLLocationManager.authorizationStatus()
        let latitude = userGPSLat.stringForKey("userLatitude")!
        let longitude = userGPSLong.stringForKey("userLongitude")!
        if (authstate == CLAuthorizationStatus.AuthorizedWhenInUse && longitude != "NO_LOCATION")
        {
            print ("Location is authorized")

            let status = checkNetworkStatus()
            
            if status == true {
        //    if Reachability.isConnectedToNetwork() == true {
                
                print ("Longitude:\(longitude)")
                print ("Latitude:\(latitude)")
                self.activityIndicator.hidden = false
                self.activityIndicator.startAnimating()
                
                let username = NSUserDefaults.standardUserDefaults()
                let loginUsername = username.stringForKey("loginUsername")
                
                var urlAsString = api_url+"business_ios_gps.php"
                urlAsString = urlAsString+"?apiKey="+apiKey+"&cust_username="+loginUsername!+"&gps=1"+"&latitude="+latitude+"&longitude="+longitude
                print (urlAsString)
                
                let url = NSURL(string: urlAsString)!
                let session = NSURLSession.sharedSession()
                
                let request = NSMutableURLRequest(URL: url)
                request.HTTPMethod = "GET"
                request.cachePolicy = NSURLRequestCachePolicy.ReloadIgnoringCacheData
                
                let task = session.dataTaskWithRequest(request) {
                    (
                    let data, let response, let error) in
                    
                    guard let _:NSData = data, let _:NSURLResponse = response  where error == nil else {
                        print("error")
                        return
                    }
          
                    dispatch_async(dispatch_get_main_queue(), {
                        let json = NSString(data: data!, encoding: NSASCIIStringEncoding)
                        self.extract_json(json!)
//                        self.activityIndicator.stopAnimating()
//                        self.activityIndicator.hidden = true
                        return
                    })
                    
                }
                
                task.resume()
                
                
            }
            else {
                print("Internet connection FAILED")
                // alert box code below
                let title = NSLocalizedString("USER_MESSAGE9", comment: "Not Connected")
                let Message = NSLocalizedString("USER_MESSAGE10", comment: "You are not connected to internet!.")
                let OK = NSLocalizedString("USER_MESSAGE3", comment: "OK")
                
                let alert = UIAlertController(title: title, message: Message, preferredStyle: .Alert)
                let action = UIAlertAction(title: OK, style: .Default) { _ in
                    // Put here any code that you would like to execute when
                    // the user taps that OK button (may be empty in your case if that's just
                    // an informative alert
                    self.dismissViewControllerAnimated(true, completion: {})
                }
                alert.addAction(action)
                self.presentViewController(alert, animated: true, completion: nil)
                // alert box code end
            }
            
        }
            
//        else if(authstate == CLAuthorizationStatus.Denied){
//            print("Location is denied")
//            
//        }
        else {
         //   if Reachability.isConnectedToNetwork() == true {
            let status = checkNetworkStatus()
            
            if status == true {
            
                self.activityIndicator.hidden = false
                self.activityIndicator.startAnimating()
                
                let username = NSUserDefaults.standardUserDefaults()
                let loginUsername = username.stringForKey("loginUsername")
                
                var urlAsString = api_url+"business_ios.php"
                urlAsString = urlAsString+"?apiKey="+apiKey+"&cust_username="+loginUsername!
                print (urlAsString)
                
                let url = NSURL(string: urlAsString)!
                let session = NSURLSession.sharedSession()
                
                let request = NSMutableURLRequest(URL: url)
                request.HTTPMethod = "GET"
                request.cachePolicy = NSURLRequestCachePolicy.ReloadIgnoringCacheData
                
                let task = session.dataTaskWithRequest(request) {
                    (
                    let data, let response, let error) in
                    
                    guard let _:NSData = data, let _:NSURLResponse = response  where error == nil else {
                        print("error")
                        return
                    }
                 
                    dispatch_async(dispatch_get_main_queue(), {
                        let json = NSString(data: data!, encoding: NSASCIIStringEncoding)
                        self.extract_json(json!)
//                        self.activityIndicator.stopAnimating()
//                        self.activityIndicator.hidden = true
                        return
                    })
                    
                }
                
                task.resume()
                
            }
            else {
                print("Internet connection FAILED")
                // alert box code below
                let title = NSLocalizedString("USER_MESSAGE9", comment: "Not Connected")
                let Message = NSLocalizedString("USER_MESSAGE10", comment: "You are not connected to internet!.")
                let OK = NSLocalizedString("USER_MESSAGE3", comment: "OK")
                
                let alert = UIAlertController(title: title, message: Message, preferredStyle: .Alert)
                let action = UIAlertAction(title: OK, style: .Default) { _ in
                    // Put here any code that you would like to execute when
                    // the user taps that OK button (may be empty in your case if that's just
                    // an informative alert
                    self.dismissViewControllerAnimated(true, completion: {})
                }
                alert.addAction(action)
                self.presentViewController(alert, animated: true, completion: nil)
                // alert box code end
            }
        }
    }
    
    
    func extract_json(data:NSString)
        
    {
        var parseError: NSError?
        let jsonData:NSData = data.dataUsingEncoding(NSASCIIStringEncoding)!
        let json: AnyObject?
        do {
            json = try NSJSONSerialization.JSONObjectWithData(jsonData, options: [])
        } catch let error as NSError {
            parseError = error
            json = nil
        }
        if (parseError == nil)
        {
            if let list:NSArray = json as? NSArray
            {
                //                print ("Inside if let")
                //                print (list)
                //                                print ("List count\(list.count)")
                
                for (var i = 0; i < list.count; i += 1 )
                {
                    if let data_block = list[i] as? NSDictionary
                    {
                        TableData.append(datastruct(add: data_block))
                        
                    }
                }
                do_table_refresh()
                fetchGPSCoordinates()
                self.activityIndicator.stopAnimating()
                self.activityIndicator.hidden = true
            }
            
        }
        
        
    }
    func refresh(sender:AnyObject) {
        // if Reachability.isConnectedToNetwork() == true {
        let status = checkNetworkStatus()
        
        if status == true {
        self.TableData.removeAll()
        fetchLocalBusiness()
        }
         else {
            print("Internet connection FAILED")
            // alert box code below
            let title = NSLocalizedString("USER_MESSAGE9", comment: "Not Connected")
            let Message = NSLocalizedString("USER_MESSAGE10", comment: "You are not connected to internet!.")
            let OK = NSLocalizedString("USER_MESSAGE3", comment: "OK")
            
            let alert = UIAlertController(title: title, message: Message, preferredStyle: .Alert)
            let action = UIAlertAction(title: OK, style: .Default) { _ in
                // Put here any code that you would like to execute when
                // the user taps that OK button (may be empty in your case if that's just
                // an informative alert
                self.refreshControl.endRefreshing()
                self.activityIndicator.hidden = true
                self.dismissViewControllerAnimated(true, completion: {})
            }
            alert.addAction(action)
            self.presentViewController(alert, animated: true, completion: nil)
            // alert box code end

        }
        
    }
    func do_table_refresh()
    {
        dispatch_async(dispatch_get_main_queue(), {
            self.tableView.reloadData()
            self.refreshControl.endRefreshing()
             if (self.TableData.count>1) {
            self.loadDataArray()
            }
            return
        })
    }
    
    func fetchGPSCoordinates() {
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.requestWhenInUseAuthorization()
            if #available(iOS 9.0, *) {
                locationManager.requestLocation()
            } else {
                // Fallback on earlier versions
            }
            
        }
        
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        
        if segue.identifier == "businessdetail" {
            //       let indexPath: NSIndexPath = sender as! NSIndexPath
            
            if (self.resultSearchController.active) {
                
                //                let indexPath = self.searchDisplayController?.searchResultsTableView.indexPathForSelectedRow
                let indexPath = self.tableView.indexPathForSelectedRow
                if indexPath != nil {
                    let advertiserBrand = filteredTableData[indexPath!.row].brandName
                    let advertiserDescription = filteredTableData[indexPath!.row].brandDescription
                    let businessImageURL = filteredTableData[indexPath!.row].imageURL
                    let businessStreetAddress = filteredTableData[indexPath!.row].businessStreeAddress
                    let coverImageURL = filteredTableData[indexPath!.row].profile_cover_image_url
                    let businessLatitude = filteredTableData[indexPath!.row].business_latitude
                    let businessLongitude = filteredTableData[indexPath!.row].business_longitude
                    let destinationVC = segue.destinationViewController as! BusinessDetailViewController
                    destinationVC.business_name = advertiserBrand as String
                    destinationVC.business_description = advertiserDescription as String
                    destinationVC.profile_image_url = businessImageURL as String
                    destinationVC.businss_street_address = businessStreetAddress as String
                    destinationVC.cover_image_url = coverImageURL as String
                    destinationVC.business_latitude = businessLatitude as String
                    destinationVC.business_longitude = businessLongitude as String
                }
                
            }
            else {
                
                let indexPath: NSIndexPath = sender as! NSIndexPath
                
                //            let advertisementID = TableData[indexPath.row].advertisement_id
                //            let advertiserUsername = TableData[indexPath.row].advertiser_username
                let advertiserBrand = TableData[indexPath.row].advertisement_brand
                let advertiserDescription = TableData[indexPath.row].advertisement_description
                
                //            let businessAdvertisementURL = TableData[indexPath.row].advertisement_url
                //            let businessGPSLatitude = TableData[indexPath.row].advertisement_gps_latitude
                //            let businessGPSLongitude = TableData[indexPath.row].advertisement_gps_longitude
                //            let advertisementDistance = TableData[indexPath.row].advertisement_distance
                
                let advertisementStreet = TableData[indexPath.row].advertisement_street
                //            let advertisementFBURL = TableData[indexPath.row].advertiser_fb_url
                let businessImageURL = TableData[indexPath.row].imageurl
                let coverImageURL = TableData[indexPath.row].profile_cover_image_url
                let businessLatitude = TableData[indexPath.row].advertisement_gps_latitude
                let businessLongitude = TableData[indexPath.row].advertisement_gps_longitude
                
                let destinationVC = segue.destinationViewController as! BusinessDetailViewController
                
                
                
                destinationVC.business_name = advertiserBrand! as String
                destinationVC.business_description = advertiserDescription! as String
                destinationVC.profile_image_url = businessImageURL! as String
                destinationVC.businss_street_address = advertisementStreet! as String
                destinationVC.cover_image_url = coverImageURL! as String
                destinationVC.business_latitude = businessLatitude! as String
                destinationVC.business_longitude = businessLongitude! as String
                
            }
        }
        
        
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
