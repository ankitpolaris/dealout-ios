//
//  MyDealsTableViewCell.swift
//  DealOut
//
//  Created by ankit khanna on 4/27/16.
//  Copyright © 2016 Leonidas Orfanidis. All rights reserved.
//

import UIKit

class MyDealsTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var dealName: UILabel!
    @IBOutlet weak var dealDescription: UILabel!
    @IBOutlet weak var dealImageView: UIImageView!
    @IBOutlet weak var couponStatus: UILabel!
    @IBOutlet weak var couponStatusView: UIView!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
