//
//  ScratchViewController.swift
//  DealOut
//
//  Created by ankit khanna on 4/6/16.
//  Copyright © 2016 Leonidas Orfanidis. All rights reserved.
//

import UIKit

class ScratchViewController: UIViewController {

    @IBOutlet weak var businessImageView: UIImageView!
    @IBOutlet weak var couponCode: UILabel!
    @IBOutlet weak var dealNameLabel: UILabel!
    @IBOutlet weak var dealDescriptionLabel: UILabel!
    @IBOutlet weak var circleView: UIView!
    @IBOutlet weak var scratchView: UIView!
    @IBOutlet weak var scratchHereLabel: UILabel!

    
    
    var dealID:String = ""
    var imageurl:String = ""
    var image:UIImage? = nil
    var dealName:String = ""
    var dealDescription:String = ""
    var dealCouponCode:String = ""
    var dealCouponStatus:Int = 0
    var coming_from:String = ""
    
    let dateWhenDealTakenByUser = NSUserDefaults.standardUserDefaults()
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        
//        self.businessImageView.layer.borderWidth = 2
//        self.businessImageView.layer.borderColor = UIColor.yellowColor().CGColor

        if (dealCouponStatus==1) {
            self.scratchView.hidden = true
        }
        
        self.dealNameLabel.text = dealName
        self.dealDescriptionLabel.text = dealDescription
        self.couponCode.text = dealCouponCode

        let fileUrl = NSURL(string: imageurl as String)
        self.businessImageView.layer.cornerRadius = 65
        self.businessImageView.hnk_setImageFromURL(fileUrl!)
        self.businessImageView.layer.cornerRadius = 65
        self.businessImageView.backgroundColor = UIColor(white: 1, alpha: 0.5)
            self.businessImageView.layer.cornerRadius = 65

        
     
//        self.circleView.layer.cornerRadius = 65
        self.circleView.layer.borderWidth = 2
        self.circleView.layer.borderColor = UIColor.redColor().CGColor

        
        
        // Swipe gesture code
        let leftSwipe = UISwipeGestureRecognizer(target: self, action: #selector(ScratchViewController.handleSwipes(_:)))
        let rightSwipe = UISwipeGestureRecognizer(target: self, action: #selector(ScratchViewController.handleSwipes(_:)))
        
        leftSwipe.direction = .Left
        rightSwipe.direction = .Right
        
        self.scratchView.addGestureRecognizer(leftSwipe)
        self.scratchView.addGestureRecognizer(rightSwipe)
        // End swipe gesture code
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func close(sender: UIButton) {
        
        self.dismissViewControllerAnimated(true, completion: {})
    }
    
    
    func handleSwipes(sender:UISwipeGestureRecognizer) {
        if (sender.direction == .Left) {
            print("Swipe Left")
         
            self.scratchView.alpha = self.scratchView.alpha - 0.2
            if (self.scratchView.alpha < 0.05) {
                let todaysDate:NSDate = NSDate()
                let dateFormatter:NSDateFormatter = NSDateFormatter()
                dateFormatter.dateFormat = "yyyy-MM-dd"
                let todayString:String = dateFormatter.stringFromDate(todaysDate)
                print ("Todays date: \(todayString)")
                    self.dateWhenDealTakenByUser.setObject("\(todayString)", forKey: "dateWhenDealTakenByUser")
                if (coming_from == "ALL_DEALS") {
               updateAllDealAsTaken()
                }
                else if (coming_from == "MY_DEALS") {
                    updateMyDealAsTaken()
                }
                
            }
        }
        
        if (sender.direction == .Right) {
            print("Swipe Right")

            self.scratchHereLabel.hidden = true
            self.scratchView.alpha = self.scratchView.alpha - 0.2
            if (self.scratchView.alpha < 0.05) {
                let todaysDate:NSDate = NSDate()
                let dateFormatter:NSDateFormatter = NSDateFormatter()
                dateFormatter.dateFormat = "yyyy-MM-dd"
                let todayString:String = dateFormatter.stringFromDate(todaysDate)
                print ("Todays date: \(todayString)")
                self.dateWhenDealTakenByUser.setObject("\(todayString)", forKey: "dateWhenDealTakenByUser")
                
                if (coming_from == "ALL_DEALS") {
                    updateAllDealAsTaken()
                }
                else if (coming_from == "MY_DEALS") {
                    updateMyDealAsTaken()
                }            }
        }
    }
    func updateMyDealAsTaken() {
         print ("My Deal is now taken.")
      //  if Reachability.isConnectedToNetwork() == true{
        let status = checkNetworkStatus()
        
        if status == true {
            
            
            
            let apiURL = api_url+"updatemydeal.php"
            
            let request = NSMutableURLRequest(URL: NSURL(string: apiURL)!)
            request.HTTPMethod = "POST"
            let postString = "deal_id=\(self.dealID)&apiKey=\(apiKey)&consume_flag=1"
            request.HTTPBody = postString.dataUsingEncoding(NSUTF8StringEncoding)
            let configuration = NSURLSessionConfiguration.defaultSessionConfiguration() // added for fix
            let session = NSURLSession(configuration: configuration, delegate:nil, delegateQueue:NSOperationQueue.mainQueue()) // added for fix
            let task = session.dataTaskWithRequest(request) {
                data, response, error in
                
                if error != nil {
                    print("Errrrrrrr.......... error=\(error)")
                    return
                }
                
                print("responseString = \(response)")
                
                let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)!
                print("responseString_username = \(responseString)")
                
                
                do {
                    let json = try NSJSONSerialization.JSONObjectWithData(data!, options: .AllowFragments)
                    
                    let response = json["response"] as! String
                    
                    
                    print ("Final Response: \(response)")
                    
                    if (response == "UPDATE_SUCCESS")    {
                        
                        print("DEAL_STATUS_UPDATED")
                        
                        
                        
                        
                    }
                    else {
                        
                        print("DEAL_UPDATE_FAIL")
                        //                        let alert = UIAlertView(title: "Error", message: "Invalid current Password or password do not match. Please enter again.", delegate: nil, cancelButtonTitle: "OK")
                        //                        alert.show()
                        // alert box code below
                        let title = NSLocalizedString("USER_MESSAGE11", comment: "Error")
                        let Message = NSLocalizedString("USER_MESSAGE12", comment: "There's an error in taking deal, please try again.")
                        let OK = NSLocalizedString("USER_MESSAGE3", comment: "OK")
                        
                        let alert = UIAlertController(title: title, message: Message, preferredStyle: .Alert)
                        let action = UIAlertAction(title: OK, style: .Default) { _ in
                            // Put here any code that you would like to execute when
                            // the user taps that OK button (may be empty in your case if that's just
                            // an informative alert
                        }
                        alert.addAction(action)
                        self.presentViewController(alert, animated: true, completion: nil)
                        // alert box code end
                        
                    }
                    
                    
                    
                } catch {
                    print("error serializing JSON: \(error)")
                }
  
            }
            task.resume()
        }
            
        else {
            // alert box code below
            let alert = UIAlertController(title: "Not Connected", message:"You are not connected to internet!.", preferredStyle: .Alert)
            let action = UIAlertAction(title: "OK", style: .Default) { _ in
                // Put here any code that you would like to execute when
                // the user taps that OK button (may be empty in your case if that's just
                // an informative alert
                self.dismissViewControllerAnimated(true, completion: {})
            }
            alert.addAction(action)
            self.presentViewController(alert, animated: true, completion: nil)
            // alert box code end
        }

    }
    func updateAllDealAsTaken() {
        
        print ("Deal is now taken.")
     //   if Reachability.isConnectedToNetwork() == true {
        let status = checkNetworkStatus()
        
        if status == true {

            let username = NSUserDefaults.standardUserDefaults()
            let loginUsername = username.stringForKey("loginUsername")
        
            let apiURL = api_url+"updatealldeal_ios.php"
            
            let request = NSMutableURLRequest(URL: NSURL(string: apiURL)!)
            request.HTTPMethod = "POST"
            let postString = "deal_id=\(self.dealID)&consumed_by_username=\(loginUsername!)&apiKey=\(apiKey)"
            request.HTTPBody = postString.dataUsingEncoding(NSUTF8StringEncoding)
            let configuration = NSURLSessionConfiguration.defaultSessionConfiguration() // added for fix
            let session = NSURLSession(configuration: configuration, delegate:nil, delegateQueue:NSOperationQueue.mainQueue()) // added for fix
            let task = session.dataTaskWithRequest(request) {
                data, response, error in
                
                if error != nil {
                    print("Errrrrrrr.......... error=\(error)")
                    return
                }
                
                print("responseString = \(response)")
                
                let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)!
                print("responseString_username = \(responseString)")
                
                
                do {
                    let json = try NSJSONSerialization.JSONObjectWithData(data!, options: .AllowFragments)
                    
                    let response = json["response"] as! String
                    
                    
                    print ("Final Response: \(response)")
                    
                    if (response == "DEAL_AVAILABLE")    {

                        print("DEAL_STATUS_UPDATED")

                        

                        
                    }
                    else {

                        print("DEAL_UPDATE_FAIL")
                        //                        let alert = UIAlertView(title: "Error", message: "Invalid current Password or password do not match. Please enter again.", delegate: nil, cancelButtonTitle: "OK")
                        //                        alert.show()
                        // alert box code below
                        let alert = UIAlertController(title: "Error", message:"There's an error in taking deal, please try again.", preferredStyle: .Alert)
                        let action = UIAlertAction(title: "OK", style: .Default) { _ in
                            // Put here any code that you would like to execute when
                            // the user taps that OK button (may be empty in your case if that's just
                            // an informative alert
                        }
                        alert.addAction(action)
                        self.presentViewController(alert, animated: true, completion: nil)
                        // alert box code end
                        
                    }
                    
                    
                    
                } catch {
                    print("error serializing JSON: \(error)")
                }
                
                
                
            }
            task.resume()


        }
        else {
            // alert box code below
            let alert = UIAlertController(title: "Not Connected", message:"You are not connected to internet!.", preferredStyle: .Alert)
            let action = UIAlertAction(title: "OK", style: .Default) { _ in
                // Put here any code that you would like to execute when
                // the user taps that OK button (may be empty in your case if that's just
                // an informative alert
                self.dismissViewControllerAnimated(true, completion: {})
            }
            alert.addAction(action)
            self.presentViewController(alert, animated: true, completion: nil)
            // alert box code end
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
