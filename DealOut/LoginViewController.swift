//
//  LoginViewController.swift
//  DealOut
//
//  Created by ankit khanna on 2/10/16.
//  Copyright © 2016 Leonidas Orfanidis. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit
import Foundation
import SystemConfiguration

class LoginViewController: UIViewController, FBSDKLoginButtonDelegate {

    
    let facebookConnectionStatus = NSUserDefaults.standardUserDefaults()

    @IBOutlet weak var loginView: UIView!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var facebookLogin: UIButton!
    @IBOutlet weak var SignUp: UIButton!
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    
    let loginUsername = NSUserDefaults.standardUserDefaults()
    let loginPassword = NSUserDefaults.standardUserDefaults()
    let loginCustID = NSUserDefaults.standardUserDefaults()
    let loginCustFirstname = NSUserDefaults.standardUserDefaults()
    let loginCustLastname = NSUserDefaults.standardUserDefaults()
    let UserDeviceToken = NSUserDefaults.standardUserDefaults()
    override func viewDidLoad() {
        super.viewDidLoad()

        self.loginView.layer.cornerRadius = 10
        self.loginButton.layer.cornerRadius = 5
        self.loginButton.layer.borderWidth = 2.0
        self.loginButton.layer.borderColor = UIColor(red: 255.0/255.0, green: 228.0/255.0, blue: 24.0/255.0, alpha: 1.0).CGColor
        self.facebookLogin.layer.cornerRadius = 5
        self.facebookLogin.layer.borderWidth = 2.0
        self.facebookLogin.layer.borderColor = UIColor(red: 139.0/255.0, green: 157.0/255.0, blue: 195.0/255.0, alpha: 1.0).CGColor
        self.SignUp.layer.cornerRadius = 5
        self.SignUp.layer.borderWidth = 2.0
        self.SignUp.layer.borderColor = UIColor(red: 139.0/255.0, green: 157.0/255.0, blue: 195.0/255.0, alpha: 1.0).CGColor
        
        self.activityIndicator.hidden = true
        
        
        if (FBSDKAccessToken.currentAccessToken() == nil)
        {
            print("Not logged in..")
        }
        else
        {
            print("Logged in..")
            //            self.facebookConnectionStatus.setObject("Connected", forKey: "facebookConnectionStatus")
            //            NSUserDefaults.standardUserDefaults().synchronize()
            
        }
        
        let loginButton = FBSDKLoginButton()
        loginButton.readPermissions = ["public_profile", "email", "user_friends"]
        loginButton.center = self.view.center
        loginButton.delegate = self
        
        // Do any additional setup after loading the view.
    }
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.view.endEditing(true)
        //        self.scrollView.endEditing(true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func login(sender: AnyObject) {
        
            loginUser()
        
    }
    @IBAction func facebookLoginButtonClicked(sender: AnyObject) {
        
    //    if Reachability.isConnectedToNetwork() == true {
        let status = checkNetworkStatus()
        
        if status == true {
            let loginButton = FBSDKLoginButton()
            loginButton.readPermissions = ["public_profile", "email", "user_friends"]
            //        loginButton.center = self.view.center
            loginButton.delegate = self
            loginButton.sendActionsForControlEvents(.TouchUpInside)
            //let accessToken = FBSDKAccessToken.currentAccessToken()
            //     print(accessToken)
        }
        else {
            print("Internet connection FAILED")
            // alert box code below
            let title = NSLocalizedString("USER_MESSAGE9", comment: "Not Connected")
            let Message = NSLocalizedString("USER_MESSAGE10", comment: "You are not connected to internet!.")
            let OK = NSLocalizedString("USER_MESSAGE3", comment: "OK")
            
            let alert = UIAlertController(title: title, message: Message, preferredStyle: .Alert)
            let action = UIAlertAction(title: OK, style: .Default) { _ in
                // Put here any code that you would like to execute when
                // the user taps that OK button (may be empty in your case if that's just
                // an informative alert
                self.dismissViewControllerAnimated(true, completion: {})
            }
            alert.addAction(action)
            self.presentViewController(alert, animated: true, completion: nil)
            // alert box code end
        }
    }
    
    
    // MARK: - Facebook Login
    func loginButton(loginButton: FBSDKLoginButton!, didCompleteWithResult result: FBSDKLoginManagerLoginResult!, error: NSError!)
    {
        if error == nil
        {
            print("Login complete.")
            
            self.facebookConnectionStatus.setObject("Connected", forKey: "facebookConnectionStatus")
            NSUserDefaults.standardUserDefaults().synchronize()
            
          //  if Reachability.isConnectedToNetwork() == true {
            let status = checkNetworkStatus()
            
            if status == true {
                
                // Graph API to fetch Facebook User info
                let graphRequest : FBSDKGraphRequest = FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, email"])
                graphRequest.startWithCompletionHandler({ (connection, result, error) -> Void in
                    
                    if ((error) != nil)
                    {
                        // Process error
                        print("Error: \(error)")
                    }
                    else
                    {
                        print("fetched user: \(result)")
                        let userName : NSString = result.valueForKey("name") as! NSString
                        print("User Name is: \(userName)")
                        let userID : NSString = result.valueForKey("id") as! NSString
                        print("User ID is: \(userID)")
                        let userEmail : NSString = result.valueForKey("email") as! NSString
                        print("User Email is: \(userEmail)")
                        let fbUserFirstName : NSString = result.valueForKey("first_name") as! NSString
                        let fbUserLastName : NSString = result.valueForKey("last_name") as! NSString
                        
                        print("Internet connection OK")
                        // Check username validation code
                       
                        let DeviceToken = self.UserDeviceToken.stringForKey("devicetoken")
                     //   let cust_fb_reg_id = "dsadq34343423"
                        let cust_city = ""
                        let cust_address = ""
                        let cust_state = ""
                        let cust_phone = ""
                        let cust_dob = ""
                        let cust_gender = ""
                        let cust_interest = ""
                        
                        
                        let apiURL = api_url+"create_user_ios.php"
                        let cust_fb_login = 1
                        let request = NSMutableURLRequest(URL: NSURL(string: apiURL)!)
                        request.HTTPMethod = "POST"
                        let postString = "cust_firstname=\(fbUserFirstName)&cust_lastname=\(fbUserLastName)&cust_username=\(userID)&cust_password=\(userID)&cust_email=\(userEmail)&cust_reg_id=\(DeviceToken!)&apiKey=\(apiKey)&cust_fb_login=\(cust_fb_login)&cust_city=\(cust_city)&cust_address=\(cust_address)&cust_state=\(cust_state)&cust_phone=\(cust_phone)&cust_dob=\(cust_dob)&cust_gender=\(cust_gender)&cust_interest=\(cust_interest)&ios_flag=1"
                        print (postString)
                        request.HTTPBody = postString.dataUsingEncoding(NSUTF8StringEncoding)
                        let configuration = NSURLSessionConfiguration.defaultSessionConfiguration() // added for fix
                        let session = NSURLSession(configuration: configuration, delegate:nil, delegateQueue:NSOperationQueue.mainQueue()) // added for fix
                        let task = session.dataTaskWithRequest(request) {
                            data, response, error in
                            
                            if error != nil {
                                print("Errrrrrrr.......... error=\(error)")
                                return
                            }
                            
                            print("responseString = \(response)")
                            
                            let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)!
                            print("responseString_username = \(responseString)")
                            
                            
                            do {
                                let json = try NSJSONSerialization.JSONObjectWithData(data!, options: .AllowFragments)
                                
                                let response = json["response"] as! String
                                let cust_id = json["cust_id"] as! String
                                
                                print ("Final Response: \(response)")
                                
                                if (response == "USER_CREATED_SUCCESSFULLY")    {
                                    self.activityIndicator.stopAnimating()
                                    self.activityIndicator.hidden = true
                                    print("USER_CREATED_SUCCESSFULLY")
                                    
                                    self.loginUsername.setObject("\(userID)", forKey: "loginUsername")
                                    self.loginPassword.setObject("\(userID)", forKey: "loginPassword")
                                    self.loginCustID.setObject("\(cust_id)", forKey: "loginCustID")
                                    
                                    // alert box code below
                                    if let pageViewController =
                                        self.storyboard?.instantiateViewControllerWithIdentifier("WalkthroughController")
                                            as? WalkthroughPageViewController {
                                        self.presentViewController(pageViewController, animated: true, completion:
                                            nil)
                                    }
                                    // alert box code end
                                    
                                }
                                else {
                                    self.activityIndicator.stopAnimating()
                                    self.activityIndicator.hidden = true
                                    print("USERNAME_ALREADY_EXISTS")
                                    //                        let alert = UIAlertView(title: "Error", message: "Invalid current Password or password do not match. Please enter again.", delegate: nil, cancelButtonTitle: "OK")
                                    //                        alert.show()
                                    self.loginUsername.setObject("\(userID)", forKey: "loginUsername")
                                    self.loginPassword.setObject("\(userID)", forKey: "loginPassword")
                                    self.loginCustID.setObject("\(cust_id)", forKey: "loginCustID")
                                    
                                    // alert box code below
                                    if let pageViewController =
                                        self.storyboard?.instantiateViewControllerWithIdentifier("WalkthroughController")
                                            as? WalkthroughPageViewController {
                                        self.presentViewController(pageViewController, animated: true, completion:
                                            nil)
                                    }
                                    // alert box code end
                                    
                                }
                                
                                
                                
                            } catch {
                                print("error serializing JSON: \(error)")
                            }
                            
                            
                            
                        }
                        task.resume()
                        

                        
                    }
                })
                
                // End of Graph API Fetch user info
                
                
                
                
                
                //  self.performSegueWithIdentifier("menu", sender: self)
            }
            else {
                print("Internet connection FAILED")
                // alert box code below
                let title = NSLocalizedString("USER_MESSAGE9", comment: "Not Connected")
                let Message = NSLocalizedString("USER_MESSAGE10", comment: "You are not connected to internet!.")
                let OK = NSLocalizedString("USER_MESSAGE3", comment: "OK")
                
                let alert = UIAlertController(title: title, message: Message, preferredStyle: .Alert)
                let action = UIAlertAction(title: OK, style: .Default) { _ in
                    // Put here any code that you would like to execute when
                    // the user taps that OK button (may be empty in your case if that's just
                    // an informative alert
                    self.dismissViewControllerAnimated(true, completion: {})
                }
                alert.addAction(action)
                self.presentViewController(alert, animated: true, completion: nil)
                // alert box code end
            }
        }
            
        else
        {
            print(error.localizedDescription)
        }
    }
    func loginButtonDidLogOut(loginButton: FBSDKLoginButton!)
    {
        print("User logged out...")
    }

    
 
    
    func loginUser () {
        
        if (usernameTextField.text!.isEmpty || passwordTextField.text!.isEmpty) {
            let title = NSLocalizedString("USER_MESSAGE16", comment: "Please enter your Username and Password")
           
            let dimiss = NSLocalizedString("USER_MESSAGE17", comment: "Dismiss")
            
            let alertController = UIAlertController(title: title, message:
                "", preferredStyle: UIAlertControllerStyle.Alert)
            alertController.addAction(UIAlertAction(title: dimiss, style: UIAlertActionStyle.Default,handler: nil))
            
            self.presentViewController(alertController, animated: true, completion: nil)
        }
        else {
            
         //   if Reachability.isConnectedToNetwork() == true {
            let status = checkNetworkStatus()
            
            if status == true {
                let userName = self.usernameTextField.text
                let password = self.passwordTextField.text
                
                let DeviceToken = self.UserDeviceToken.stringForKey("devicetoken")
                //let cust_fb_reg_id = "dsadq34343423"
                let apiURL = api_url+"login_ios.php"

                
                let request = NSMutableURLRequest(URL: NSURL(string: apiURL)!)
                request.HTTPMethod = "POST"
                let postString = "cust_username=\(userName!)&cust_password=\(password!)&cust_reg_id=\(DeviceToken!)&apiKey=\(apiKey)&ios_flag=1"
                request.HTTPBody = postString.dataUsingEncoding(NSUTF8StringEncoding)
                let configuration = NSURLSessionConfiguration.defaultSessionConfiguration() // added for fix
                let session = NSURLSession(configuration: configuration, delegate:nil, delegateQueue:NSOperationQueue.mainQueue()) // added for fix
                let task = session.dataTaskWithRequest(request) {
                    data, response, error in
                    
                    if error != nil {
                        print("Errrrrrrr.......... error=\(error)")
                        return
                    }
                    
                    print("responseString = \(response)")
                    
                    let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)!
                    print("responseString_username = \(responseString)")
                    
                    
                    do {
                        let json = try NSJSONSerialization.JSONObjectWithData(data!, options: .AllowFragments)
                        
                        let response = json["response"] as? String
                        let cust_id = json["cust_id"] as? String
                        let cust_firstname = json["cust_firstname"] as? String
                        let cust_lastname = json["cust_lastname"] as? String
                        
                        print ("Final Response: \(response)")
                        
                        if (response == "LOGIN_SUCCESS")    {
                            self.activityIndicator.stopAnimating()
                            self.activityIndicator.hidden = true
                            print("LOGIN_SUCCESS")
                            
                            self.loginUsername.setObject("\(userName!)", forKey: "loginUsername")
                            self.loginPassword.setObject("\(password!)", forKey: "loginPassword")
                            self.loginCustID.setObject("\(cust_id!)", forKey: "loginCustID")
                            self.loginCustFirstname.setObject("\(cust_firstname!)", forKey: "loginCustFirstname")
                            self.loginCustLastname.setObject("\(cust_lastname!)", forKey: "loginCustLastname")
                            
                            // alert box code below
                            let title = NSLocalizedString("USER_MESSAGE14", comment: "Success!")
                            let Message = NSLocalizedString("USER_MESSAGE15", comment: "Log in Succsessful")
                            let OK = NSLocalizedString("USER_MESSAGE3", comment: "OK")
                            
                            let alert = UIAlertController(title: title, message: Message, preferredStyle: .Alert)
                            let action = UIAlertAction(title: OK, style: .Default) { _ in
                                // Put here any code that you would like to execute when
                                // the user taps that OK button (may be empty in your case if that's just
                                // an informative alert
                                let walkThroughFlag = NSUserDefaults.standardUserDefaults()
                                
                                let WalkThroughFlag = walkThroughFlag.stringForKey("walkThroughFlag")
                                if (WalkThroughFlag=="1") {
                                    
                                    if let pageViewController =
                                        self.storyboard?.instantiateViewControllerWithIdentifier("AllDeals")
                                            as? FirstViewController {
                                        self.presentViewController(pageViewController, animated: true, completion:
                                            nil)
                                    }
                                    
                                }
                                else {
                                    if let pageViewController =
                                        self.storyboard?.instantiateViewControllerWithIdentifier("WalkthroughController")
                                            as? WalkthroughPageViewController {
                                        self.presentViewController(pageViewController, animated: true, completion:
                                            nil)
                                    }
                                }
                            }
                            alert.addAction(action)
                            self.presentViewController(alert, animated: true, completion: nil)
                            // alert box code end
                            
                        }
                        else {
                            self.activityIndicator.stopAnimating()
                            self.activityIndicator.hidden = true
                            print("INCORRECT_PASSWORD")
                            
                            let title = NSLocalizedString("USER_MESSAGE11", comment: "Error")
                            let Message = NSLocalizedString("USER_MESSAGE13", comment: "Username or Password incorrect.")
                            let OK = NSLocalizedString("USER_MESSAGE3", comment: "OK")
                            
                            // alert box code below
                            let alert = UIAlertController(title: title, message: Message, preferredStyle: .Alert)
                            let action = UIAlertAction(title: OK, style: .Default) { _ in
                                // Put here any code that you would like to execute when
                                // the user taps that OK button (may be empty in your case if that's just
                                // an informative alert
                            }
                            alert.addAction(action)
                            self.presentViewController(alert, animated: true, completion: nil)
                            // alert box code end
                            
                        }
                        
                        
                        
                    } catch {
                        print("error serializing JSON: \(error)")
                    }
                    
                    
                    
                }
                task.resume()

                
                
                
                
            }
            else {
                print("Internet connection FAILED")
                // alert box code below
                let title = NSLocalizedString("USER_MESSAGE9", comment: "Not Connected")
                let Message = NSLocalizedString("USER_MESSAGE10", comment: "You are not connected to internet!.")
                let OK = NSLocalizedString("USER_MESSAGE3", comment: "OK")
                
                let alert = UIAlertController(title: title, message: Message, preferredStyle: .Alert)
                let action = UIAlertAction(title: OK, style: .Default) { _ in
                    // Put here any code that you would like to execute when
                    // the user taps that OK button (may be empty in your case if that's just
                    // an informative alert
                    self.dismissViewControllerAnimated(true, completion: {})
                }
                alert.addAction(action)
                self.presentViewController(alert, animated: true, completion: nil)
                // alert box code end
            }
            
        }
            
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
