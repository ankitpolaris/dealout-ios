//
//  AllDealsTableViewCell.swift
//  DealOut
//
//  Created by ankit khanna on 3/23/16.
//  Copyright © 2016 Leonidas Orfanidis. All rights reserved.
//

import UIKit

class AllDealsTableViewCell: UITableViewCell {

    @IBOutlet weak var couponStatus: UILabel!
    @IBOutlet weak var dealsImageView: UIImageView!
    @IBOutlet weak var dealName: UILabel!
    @IBOutlet weak var dealDescription: UILabel!
    @IBOutlet weak var dealCouponStatusView: UIView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
