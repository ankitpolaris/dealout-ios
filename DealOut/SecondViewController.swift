//
//  SecondViewController.swift
//  DealOut
//
//  Created by ankit khanna on 2/10/16.
//  Copyright © 2016 Leonidas Orfanidis. All rights reserved.
//

import UIKit



class SecondViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource, UISearchResultsUpdating {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    lazy var refreshControl = UIRefreshControl()


    var TableData:Array< datastruct > = Array < datastruct >()
    
    enum ErrorHandler:ErrorType
    {
        case ErrorFetchingResults
    }
    
    struct datastruct
    {
        var imageurl:String?
        var image:UIImage? = nil
        var deal_id:String?
        var deal_name:String?
        var consume_flag:Int?
        var deal_description:String?
        var coupon_code:String?
        var expiry_date:String?
        var deal_gps_latitude:String?
        var deal_gps_longitude:String?
        var deal_gps_distance:String?
        var kind_of_business:String?
        var deal_coupon_code:String?
        var count_deals:String?
        
        
        init(add: NSDictionary)
        {
            imageurl = add["cust_image"] as? String
            deal_id = add["deal_id"] as? String
            deal_name = add["deal_name"] as? String
            consume_flag = add["consume_flag"] as? Int
            deal_description = add["cust_message"] as? String
            coupon_code = add["coupon_code"] as? String
            expiry_date = add["expiry_date"] as? String
            deal_gps_latitude = add["deal_gps_latitude"] as? String
            deal_gps_longitude = add["deal_gps_longitude"] as? String
            deal_gps_distance = add["deal_gps_distance"] as? String
            kind_of_business = add["kind_of_business"] as? String
            deal_coupon_code = add["coupon_code"] as? String
            count_deals = add["count_deals"] as? String
            
        }
    }
    
    var filteredTableData :[(dealName:String , dealDescription: String, imageURL: String, dealCouponStatus: Int, dealCouponCode: String, dealID: String)] = []
    var brandAndBrandDescription1: [(dealName:String , dealDescription: String, imageURL: String, dealCouponStatus: Int, dealCouponCode: String, dealID: String)] = []
    
    var resultSearchController = UISearchController()

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.activityIndicator.hidden = true
        self.refreshControl = UIRefreshControl()
        self.refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        self.refreshControl.addTarget(self, action: "refresh:", forControlEvents: UIControlEvents.ValueChanged)
        tableView.addSubview(refreshControl)
        
        fetchMyDeals()
        addSlideMenuButton()
        
        if #available(iOS 9.0, *) {
            self.resultSearchController.loadViewIfNeeded()// iOS 9
        } else {
            // Fallback on earlier versions
            let _ = self.resultSearchController.view          // iOS 8
        }
        
        self.resultSearchController = ({
            let controller = UISearchController(searchResultsController: nil)
            controller.searchResultsUpdater = self
            controller.dimsBackgroundDuringPresentation = false
            controller.searchBar.sizeToFit()
            self.tableView.tableHeaderView = controller.searchBar
            
            return controller
        })()
        // Reload the table
        self.tableView.reloadData()
        
    }
    override func viewWillAppear(animated: Bool) {
        let logo = UIImage(named: "dealout_ios_logo") as UIImage?
        let imageView = UIImageView(image:logo)
        
        imageView.frame.size.width = 60;
        imageView.frame.size.height = 25;
        imageView.contentMode = UIViewContentMode.Center
        
        self.navigationItem.titleView = imageView
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (self.resultSearchController.active) {
            return self.filteredTableData.count
        }
        else {
            return TableData.count
        }
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let countDeals = TableData[indexPath.row].count_deals
       // print ("CountDeals : \(countDeals!)")
        if (countDeals!=="NO_DEALS") {
            let cell: NoDealsTableViewCell = tableView.dequeueReusableCellWithIdentifier("nodeals", forIndexPath: indexPath) as! NoDealsTableViewCell
            return cell

        }
        else {
            
          let cell: MyDealsTableViewCell = tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath) as! MyDealsTableViewCell
            
            cell.couponStatus.transform = CGAffineTransformMakeRotation(CGFloat(M_PI_2));
            if (self.resultSearchController.active) {
                cell.dealName.text = filteredTableData[indexPath.row].dealName
                cell.dealDescription.text = filteredTableData[indexPath.row].dealDescription
                let couponFlag = filteredTableData[indexPath.row].dealCouponStatus
                print ("Coupon Flag: \(couponFlag)")
                if (couponFlag==0) {
                    let lightBlueColor = UIColor(red: 70.0/255.0, green: 192.0/255.0, blue: 201.0/255.0, alpha: 1.0)
                    cell.couponStatusView.backgroundColor = lightBlueColor
                    cell.couponStatus.text = "AVAILABLE"
                }
                else if (couponFlag==1) {
                    cell.couponStatus.text = "TAKEN"
                    cell.couponStatusView.backgroundColor = UIColor.redColor()
                }
                let imageURL = filteredTableData[indexPath.row].imageURL
                let fileUrl = NSURL(string: imageURL as String)
                cell.dealImageView.layer.cornerRadius = 40
                cell.dealImageView.hnk_setImageFromURL(fileUrl!)
                
                return cell
            }
            else {
        cell.dealName.text = TableData[indexPath.row].deal_name
        cell.dealDescription.text = TableData[indexPath.row].deal_description
    
        let couponFlag = TableData[indexPath.row].consume_flag
        if (couponFlag==0) {
            let lightBlueColor = UIColor(red: 70.0/255.0, green: 192.0/255.0, blue: 201.0/255.0, alpha: 1.0)
            cell.couponStatusView.backgroundColor = lightBlueColor
            cell.couponStatus.text = "AVAILABLE"
        }
        else if (couponFlag==1) {
            cell.couponStatus.text = "TAKEN"
            cell.couponStatusView.backgroundColor = UIColor.redColor()
        }
            let imageURL = TableData[indexPath.row].imageurl
            let fileUrl = NSURL(string: imageURL! as String)
            cell.dealImageView.layer.cornerRadius = 40
            cell.dealImageView.hnk_setImageFromURL(fileUrl!)

       
        return cell
            }
        }
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        self.performSegueWithIdentifier("scratchmy", sender: indexPath)
        
    }
    func updateSearchResultsForSearchController(searchController: UISearchController) {
        guard let searchString = searchController.searchBar.text else {
            return
        }
        
        filteredTableData = brandAndBrandDescription1.filter({ (country) -> Bool in
            let countryText:NSString = country.dealName
            
            return (countryText.rangeOfString(searchString, options: NSStringCompareOptions.CaseInsensitiveSearch).location) != NSNotFound
        })
        
        // Reload the tableview.
        self.tableView.reloadData()
    }
    func fetchMyDeals() {
        let status = checkNetworkStatus()
        
        if status == true {
        //if Reachability.isConnectedToNetwork() == true {
            self.activityIndicator.hidden = false
            self.activityIndicator.startAnimating()
            
            let username = NSUserDefaults.standardUserDefaults()
            let loginUsername = username.stringForKey("loginUsername")
            
            var urlAsString = api_url+"mydeals_ios.php"
            urlAsString = urlAsString+"?apiKey="+apiKey+"&cust_username="+loginUsername!
            print (urlAsString)
            
            let url = NSURL(string: urlAsString)!
            let session = NSURLSession.sharedSession()
            
            let request = NSMutableURLRequest(URL: url)
            request.HTTPMethod = "GET"
            request.cachePolicy = NSURLRequestCachePolicy.ReloadIgnoringCacheData
            
            let task = session.dataTaskWithRequest(request) {
                (
                let data, let response, let error) in
                
                guard let _:NSData = data, let _:NSURLResponse = response  where error == nil else {
                    print("error")
                    return
                }
                self.activityIndicator.hidden = false
                self.activityIndicator.startAnimating()
                dispatch_async(dispatch_get_main_queue(), {
                    let json = NSString(data: data!, encoding: NSASCIIStringEncoding)
                    self.extract_json(json!)
                    self.activityIndicator.stopAnimating()
                    self.activityIndicator.hidden = true
                    return
                })
                
            }
            
            task.resume()
            
        }
        else {
            
            print("Internet connection FAILED")
            // alert box code below
            let title = NSLocalizedString("USER_MESSAGE9", comment: "Not Connected")
            let Message = NSLocalizedString("USER_MESSAGE10", comment: "You are not connected to internet!.")
            let OK = NSLocalizedString("USER_MESSAGE3", comment: "OK")
            
            let alert = UIAlertController(title: title, message: Message, preferredStyle: .Alert)
            let action = UIAlertAction(title: OK, style: .Default) { _ in
                // Put here any code that you would like to execute when
                // the user taps that OK button (may be empty in your case if that's just
                // an informative alert
                self.dismissViewControllerAnimated(true, completion: {})
            }
            alert.addAction(action)
            self.presentViewController(alert, animated: true, completion: nil)
            // alert box code end
        }
        
    }
    
    func extract_json(data:NSString)
        
    {
        var parseError: NSError?
        let jsonData:NSData = data.dataUsingEncoding(NSASCIIStringEncoding)!
        let json: AnyObject?
        do {
            json = try NSJSONSerialization.JSONObjectWithData(jsonData, options: [])
        } catch let error as NSError {
            parseError = error
            json = nil
        }
        if (parseError == nil)
        {
            if let list:NSArray = json as? NSArray
            {
                //                print ("Inside if let")
                //                print (list)
                //                                print ("List count\(list.count)")
                
                for (var i = 0; i < list.count; i += 1 )
                {
                    if let data_block = list[i] as? NSDictionary
                    {
                        TableData.append(datastruct(add: data_block))
                    }
                }
                do_table_refresh()
            }
            
        }
        
        
    }
    
    func refresh(sender:AnyObject) {
     //   if Reachability.isConnectedToNetwork() == true {
        let status = checkNetworkStatus()
        
        if status == true {
        self.TableData.removeAll()
        fetchMyDeals()
        }
        else {
            print("Internet connection FAILED")
            // alert box code below
            let title = NSLocalizedString("USER_MESSAGE9", comment: "Not Connected")
            let Message = NSLocalizedString("USER_MESSAGE10", comment: "You are not connected to internet!.")
            let OK = NSLocalizedString("USER_MESSAGE3", comment: "OK")
            
            let alert = UIAlertController(title: title, message: Message, preferredStyle: .Alert)
            let action = UIAlertAction(title: OK, style: .Default) { _ in
                // Put here any code that you would like to execute when
                // the user taps that OK button (may be empty in your case if that's just
                // an informative alert
                self.refreshControl.endRefreshing()
                self.dismissViewControllerAnimated(true, completion: {})
            }
            alert.addAction(action)
            self.presentViewController(alert, animated: true, completion: nil)
            // alert box code end
        }
    }
    func do_table_refresh()
    {
        dispatch_async(dispatch_get_main_queue(), {
            self.tableView.reloadData()
            self.refreshControl.endRefreshing()
            if (self.TableData.count>1) {
            self.loadDataArray()
            }
            return
        })
    }
    func loadDataArray() {
        
        for i in 0...TableData.count-1 {
            //                        print ("I:\(i)")
            //                        print ("Table Count:\(TableData.count)")
            let DealID = TableData[i].deal_id
            let DealName = TableData[i].deal_name
            let DealDescription = TableData[i].deal_description
            let BusinessImageURL = TableData[i].imageurl
            let DealCouponStatus = TableData[i].consume_flag
            let DealCouponCode = TableData[i].deal_coupon_code
            
            
            
            brandAndBrandDescription1.append(dealName: DealName! , dealDescription: DealDescription!, imageURL: BusinessImageURL!, dealCouponStatus: DealCouponStatus!, dealCouponCode: DealCouponCode!, dealID: DealID!)
            
        }
    }
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        
        if segue.identifier == "scratchmy" {
            
            if (self.resultSearchController.active) {
                
                let indexPath = self.tableView.indexPathForSelectedRow
                if indexPath != nil {
                    let dealID = filteredTableData[indexPath!.row].dealID
                    let dealName = filteredTableData[indexPath!.row].dealName
                    let dealDescription = filteredTableData[indexPath!.row].dealDescription
                    let dealCouponCode = filteredTableData[indexPath!.row].dealCouponCode
                    let dealImageURL = filteredTableData[indexPath!.row].imageURL
                    let comingFrom = "MY_DEALS"
                    let destinationVC = segue.destinationViewController as! ScratchViewController
                    
                    destinationVC.dealID = dealID as String
                    destinationVC.dealName = dealName as String
                    destinationVC.dealDescription = dealDescription as String
                    destinationVC.dealCouponCode = dealCouponCode as String
                    destinationVC.imageurl = dealImageURL as String
                    destinationVC.coming_from = comingFrom as String
                    
                }
                
                
            }
            else {
            
            let indexPath: NSIndexPath = sender as! NSIndexPath
            
            let dealID = TableData[indexPath.row].deal_id
            let dealName = TableData[indexPath.row].deal_name
            let dealDescription = TableData[indexPath.row].deal_description
            let dealCouponCode = TableData[indexPath.row].deal_coupon_code
            let dealImageURL = TableData[indexPath.row].imageurl
            let comingFrom = "MY_DEALS"
            let destinationVC = segue.destinationViewController as! ScratchViewController
            
            destinationVC.dealID = dealID! as String
            destinationVC.dealName = dealName! as String
            destinationVC.dealDescription = dealDescription! as String
            destinationVC.dealCouponCode = dealCouponCode! as String
            destinationVC.imageurl = dealImageURL! as String
            destinationVC.coming_from = comingFrom as String
            }
            
        }
        
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

