//
//  WalkthroughContentViewController.swift
//  DealOut
//
//  Created by ankit khanna on 3/5/16.
//  Copyright © 2016 Leonidas Orfanidis. All rights reserved.
//

import UIKit

class WalkthroughContentViewController: UIViewController {
    @IBOutlet weak var contentLabel: UILabel!
    @IBOutlet weak var contentImageView: UIImageView!
    @IBOutlet weak var headingLabel: UILabel!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var nextButton: UIButton!

    let walkThroughFlag = NSUserDefaults.standardUserDefaults()

    var index = 0
    var heading = ""
    var imageFile = ""
    var content = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.nextButton.layer.cornerRadius = 5
        self.nextButton.layer.borderColor = UIColor.whiteColor().CGColor
        self.nextButton.layer.borderWidth = 1.0

        
        headingLabel.text = heading
        contentLabel.text = content
        contentImageView.image = UIImage(named: imageFile)
        pageControl.currentPage = index
        let nextTitle = NSLocalizedString("USER_MESSAGE26", comment: "NEXT")
        let getStartedTitle = NSLocalizedString("USER_MESSAGE27", comment: "GET STARTED")
        switch index {
        case 0...3: nextButton.setTitle(nextTitle, forState: UIControlState.Normal)
            
        case 4: nextButton.setTitle(getStartedTitle, forState: UIControlState.Normal)
        default: break
        }
        
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func nextButtonTapped(sender: UIButton) {

        switch index {
    case 0...3:
        let pageViewController = parentViewController as!
    WalkthroughPageViewController
    pageViewController.forward(index)
    case 4:
        let flag = 1
            self.walkThroughFlag.setObject("\(flag)", forKey: "walkThroughFlag")
            self.performSegueWithIdentifier("home", sender: self)
            
    default: break
        }
        
    }
    

//    func forward(index:Int) {
//        if let nextViewController = viewControllerAtIndex(index + 1) {
//        setViewControllers([nextViewController], direction: .Forward, animated:true, completion: nil)
//        }
//    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
