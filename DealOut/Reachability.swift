//
//  Reachability.swift
//  themostplayed
//
//  Created by ankit khanna on 9/4/15.
//  Copyright (c) 2015 Michael Dunn. All rights reserved.
//
import SystemConfiguration
//
//public class Reachability {
//    class func isConnectedToNetwork() -> Bool {
//        var zeroAddress = sockaddr_in()
//        zeroAddress.sin_len = UInt8(sizeofValue(zeroAddress))
//        zeroAddress.sin_family = sa_family_t(AF_INET)
//        let defaultRouteReachability = withUnsafePointer(&zeroAddress) {
//            SCNetworkReachabilityCreateWithAddress(nil, UnsafePointer($0))
//        }
//        var flags = SCNetworkReachabilityFlags()
//        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
//            return false
//        }
//        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
//        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
//        return (isReachable && !needsConnection)
//    }
//}

//MARK: reachability class
func checkNetworkStatus() -> Bool {
    let reachability: Reachability = Reachability.reachabilityForInternetConnection()
    let networkStatus = reachability.currentReachabilityStatus().rawValue;
    var isAvailable  = false;
    
    switch networkStatus {
    case (NotReachable.rawValue):
        isAvailable = false;
        break;
    case (ReachableViaWiFi.rawValue):
        isAvailable = true;
        break;
    case (ReachableViaWWAN.rawValue):
        isAvailable = true;
        break;
    default:
        isAvailable = false;
        break;
    }
    return isAvailable;
}