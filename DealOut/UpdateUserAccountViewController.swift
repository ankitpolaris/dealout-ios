//
//  UpdateUserAccountViewController.swift
//  DealOut
//
//  Created by ankit khanna on 5/13/16.
//  Copyright © 2016 Leonidas Orfanidis. All rights reserved.
//

import UIKit

class UpdateUserAccountViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    
    @IBOutlet weak var FirstNameTextFieldView: UIView!
    @IBOutlet weak var lastnameTextFieldView: UIView!
    @IBOutlet weak var usernameTextFieldView: UIView!
    @IBOutlet weak var PasswordTextFieldView: UIView!
    @IBOutlet weak var ConfirmPasswordTextFieldView: UIView!
    @IBOutlet weak var PhoneNumberTextFieldView: UIView!
    @IBOutlet weak var EmailIdTextFieldView: UIView!
    @IBOutlet weak var AddressTextFieldView: UIView!
    @IBOutlet weak var CityTextFieldView: UIView!
    @IBOutlet weak var StateTextFieldView: UIView!
    @IBOutlet weak var DateOfBirthTextFieldView: UIView!
    
    @IBOutlet weak var boyButton: UIButton!
    @IBOutlet weak var girlButton: UIButton!
    
    
    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var confirmPasswordTextField: UITextField!
    @IBOutlet weak var phoneNumberTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var addressTextField: UITextField!
    @IBOutlet weak var cityTextField: UITextField!
    @IBOutlet weak var stateTextField: UITextField!
    @IBOutlet weak var DateOfBirthTextField: UITextField!
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    // Default
    let gender = NSUserDefaults.standardUserDefaults()
    
    let loginUsername = NSUserDefaults.standardUserDefaults()
    let loginPassword = NSUserDefaults.standardUserDefaults()
    let loginCustID = NSUserDefaults.standardUserDefaults()
    var popDatePicker : PopDatePicker?
    
    
        var TableData:Array< datastruct > = Array < datastruct >()
    
    enum ErrorHandler:ErrorType
    {
        case ErrorFetchingResults
    }
    
    struct datastruct
    {
    //    var imageurl:String?
        var image:UIImage? = nil
        
        var cust_id:String?
        var cust_firstname:String?
        var cust_lastname:String?
        var cust_city:String?
        var cust_address:String?
        var cust_phone:String?
        var cust_state:String?
        var cust_email:String?
        var cust_dob:String?
        var cust_gender:String?
        var cust_interest:String?
        var cust_username:String?
        var cust_password:String?
        
        
        init(add: NSDictionary)
        {
          //  imageurl = add["deal_image"] as? String
            
            cust_id = add["cust_id"] as? String
            cust_firstname = add["cust_firstname"] as? String
            cust_lastname = add["cust_lastname"] as? String
            cust_city = add["cust_city"] as? String
            cust_address = add["cust_address"] as? String
            cust_phone = add["cust_phone"] as? String
            cust_state = add["cust_state"] as? String
            cust_email = add["cust_email"] as? String
            cust_dob = add["cust_dob"] as? String
            cust_gender = add["cust_gender"] as? String
            cust_interest = add["cust_interest"] as? String
            cust_username = add["cust_username"] as? String
            cust_password = add["cust_password"] as? String
        }
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.cancelButton.layer.borderWidth = 1
        self.cancelButton.layer.borderColor = UIColor.whiteColor().CGColor
        self.cancelButton.layer.cornerRadius = 5
        self.saveButton.layer.borderWidth = 1
        self.saveButton.layer.borderColor = UIColor.whiteColor().CGColor
        self.saveButton.layer.cornerRadius = 5
        
        self.activityIndicator.hidden = true
        
        self.FirstNameTextFieldView.layer.borderColor =  UIColor(red: 223.0/255.0, green: 223.0/255.0, blue: 223.0/255.0, alpha: 1.0).CGColor
        self.FirstNameTextFieldView.layer.cornerRadius = 5
        self.FirstNameTextFieldView.layer.borderWidth = 1.5
        
        self.usernameTextFieldView.layer.borderColor =  UIColor(red: 223.0/255.0, green: 223.0/255.0, blue: 223.0/255.0, alpha: 1.0).CGColor
        self.usernameTextFieldView.layer.cornerRadius = 5
        self.usernameTextFieldView.layer.borderWidth = 1.5
        
        self.lastnameTextFieldView.layer.borderColor =  UIColor(red: 223.0/255.0, green: 223.0/255.0, blue: 223.0/255.0, alpha: 1.0).CGColor
        self.lastnameTextFieldView.layer.cornerRadius = 5
        self.lastnameTextFieldView.layer.borderWidth = 1.5
        
        self.PasswordTextFieldView.layer.borderColor =  UIColor(red: 223.0/255.0, green: 223.0/255.0, blue: 223.0/255.0, alpha: 1.0).CGColor
        self.PasswordTextFieldView.layer.cornerRadius = 5
        self.PasswordTextFieldView.layer.borderWidth = 1.5
        
        self.ConfirmPasswordTextFieldView.layer.borderColor =  UIColor(red: 223.0/255.0, green: 223.0/255.0, blue: 223.0/255.0, alpha: 1.0).CGColor
        self.ConfirmPasswordTextFieldView.layer.cornerRadius = 5
        self.ConfirmPasswordTextFieldView.layer.borderWidth = 1.5
        
        self.PhoneNumberTextFieldView.layer.borderColor =  UIColor(red: 223.0/255.0, green: 223.0/255.0, blue: 223.0/255.0, alpha: 1.0).CGColor
        self.PhoneNumberTextFieldView.layer.cornerRadius = 5
        self.PhoneNumberTextFieldView.layer.borderWidth = 1.5
        
        self.EmailIdTextFieldView.layer.borderColor =  UIColor(red: 223.0/255.0, green: 223.0/255.0, blue: 223.0/255.0, alpha: 1.0).CGColor
        self.EmailIdTextFieldView.layer.cornerRadius = 5
        self.EmailIdTextFieldView.layer.borderWidth = 1.5
        
        self.AddressTextFieldView.layer.borderColor =  UIColor(red: 223.0/255.0, green: 223.0/255.0, blue: 223.0/255.0, alpha: 1.0).CGColor
        self.AddressTextFieldView.layer.cornerRadius = 5
        self.AddressTextFieldView.layer.borderWidth = 1.5
        
        self.CityTextFieldView.layer.borderColor =  UIColor(red: 223.0/255.0, green: 223.0/255.0, blue: 223.0/255.0, alpha: 1.0).CGColor
        self.CityTextFieldView.layer.cornerRadius = 5
        self.CityTextFieldView.layer.borderWidth = 1.5
        
        self.StateTextFieldView.layer.borderColor =  UIColor(red: 223.0/255.0, green: 223.0/255.0, blue: 223.0/255.0, alpha: 1.0).CGColor
        self.StateTextFieldView.layer.cornerRadius = 5
        self.StateTextFieldView.layer.borderWidth = 1.5
        
        self.DateOfBirthTextFieldView.layer.borderColor =  UIColor(red: 223.0/255.0, green: 223.0/255.0, blue: 223.0/255.0, alpha: 1.0).CGColor
        self.DateOfBirthTextFieldView.layer.cornerRadius = 5
        self.DateOfBirthTextFieldView.layer.borderWidth = 1.5
        
        popDatePicker = PopDatePicker(forTextField: DateOfBirthTextField)
        DateOfBirthTextField.delegate = self
        
        fetchUserInfo()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.view.endEditing(true)
        //        self.scrollView.endEditing(true)
    }
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true;
    }
    func resign() {
        self.DateOfBirthTextField.resignFirstResponder()
    }
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
        
        if (textField === DateOfBirthTextField) {
            resign()
            let formatter = NSDateFormatter()
            formatter.dateStyle = .MediumStyle
            formatter.timeStyle = .NoStyle
            let initDate : NSDate? = formatter.dateFromString(DateOfBirthTextField.text!)
            
            let dataChangedCallback : PopDatePicker.PopDatePickerCallback = { (newDate : NSDate, forTextField : UITextField) -> () in
                
                // here we don't use self (no retain cycle)
                forTextField.text = (newDate.ToDateMediumString() ?? "?") as String
                
            }
            
            popDatePicker!.pick(self, initDate: initDate, dataChanged: dataChangedCallback)
            return false
        }
        else {
            return true
        }
    }
    @IBAction func cancel(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: {})
    }

    @IBAction func save(sender: AnyObject) {
        
      //  updateAccount()
    }
    @IBAction func boy(sender: AnyObject) {
        gender.setObject("Male", forKey: "gender")
        self.boyButton.setImage(UIImage(named: "Boy-50.png"), forState: UIControlState.Normal);
        self.girlButton.setImage(UIImage(named: "Girl-50.png"), forState: UIControlState.Normal);
        let selectedGender = gender.stringForKey("gender")
        
        if (selectedGender == "Male")   {
            print("Male button pressed")
            
        }
        else if (selectedGender == "Female")    {
            print("Female button pressed")
        }
        
    }
    @IBAction func girl(sender: AnyObject) {
        gender.setObject("Female", forKey: "gender")
        self.girlButton.setImage(UIImage(named: "Girl-50-2.png"), forState: UIControlState.Normal);
        self.boyButton.setImage(UIImage(named: "Boy-50-2.png"), forState: UIControlState.Normal);
        let selectedGender = gender.stringForKey("gender")
        
        if (selectedGender == "Male")   {
            print("Male button pressed")
        }
        else if (selectedGender == "Female")    {
            print("Female button pressed")
            
        }
    }
    
    func fetchUserInfo() {
        let LoginUsername = loginUsername.stringForKey("loginUsername")
        let UserCustID = loginCustID.stringForKey("loginCustID")
        
       // if Reachability.isConnectedToNetwork() == true {
        let status = checkNetworkStatus()
        
        if status == true {
            self.activityIndicator.hidden = false
            self.activityIndicator.startAnimating()
            
            
            var urlAsString = api_url+"fetch_user_info_ios.php"
            urlAsString = urlAsString+"?apiKey="+apiKey+"&cust_id="+UserCustID!+"&cust_username="+LoginUsername!
            print (urlAsString)
            
            let url = NSURL(string: urlAsString)!
            let session = NSURLSession.sharedSession()
            
            let request = NSMutableURLRequest(URL: url)
            request.HTTPMethod = "GET"
            request.cachePolicy = NSURLRequestCachePolicy.ReloadIgnoringCacheData
            
            let task = session.dataTaskWithRequest(request) {
                (
                let data, let response, let error) in
                
                guard let _:NSData = data, let _:NSURLResponse = response  where error == nil else {
                    print("error")
                    return
                }
                self.activityIndicator.hidden = false
                self.activityIndicator.startAnimating()
                dispatch_async(dispatch_get_main_queue(), {
                    let json = NSString(data: data!, encoding: NSASCIIStringEncoding)
                    self.extract_json(json!)
                    self.activityIndicator.stopAnimating()
                    self.activityIndicator.hidden = true
                    return
                })
                
            }
            
            task.resume()
            
            
        }
         else {
            
            print("Internet connection FAILED")
            // alert box code below
            let alert = UIAlertController(title: "Not Connected", message:"You are not connected to internet!.", preferredStyle: .Alert)
            let action = UIAlertAction(title: "OK", style: .Default) { _ in
                // Put here any code that you would like to execute when
                // the user taps that OK button (may be empty in your case if that's just
                // an informative alert
                self.dismissViewControllerAnimated(true, completion: {})
            }
            alert.addAction(action)
            self.presentViewController(alert, animated: true, completion: nil)
            // alert box code end
            
        }
        
        
        
    }
    
    
    func extract_json(data:NSString) {
        var parseError: NSError?
        let jsonData:NSData = data.dataUsingEncoding(NSASCIIStringEncoding)!
        let json: AnyObject?
        do {
            json = try NSJSONSerialization.JSONObjectWithData(jsonData, options: [])
        } catch let error as NSError {
            parseError = error
            json = nil
        }
        if (parseError == nil)
        {
            if let list:NSArray = json as? NSArray
            {
                //                print ("Inside if let")
                //                print (list)
                //                                print ("List count\(list.count)")
                
                for (var i = 0; i < list.count; i += 1 )
                {
                    if let data_block = list[i] as? NSDictionary
                    {
                        TableData.append(datastruct(add: data_block))
                    }
                }
               // do_table_refresh()
                populateValues()
            }
            
        }
        
        
    }
    func populateValues() {
        
        
        self.firstNameTextField.text = TableData[0].cust_firstname
        self.lastNameTextField.text = TableData[0].cust_lastname
        self.usernameTextField.text = TableData[0].cust_username
        self.passwordTextField.text = TableData[0].cust_password
        self.confirmPasswordTextField.text = TableData[0].cust_password
        self.phoneNumberTextField.text = TableData[0].cust_phone
        self.emailTextField.text = TableData[0].cust_email
        self.addressTextField.text = TableData[0].cust_address
        self.cityTextField.text = TableData[0].cust_city
        self.stateTextField.text = TableData[0].cust_state
        self.DateOfBirthTextField.text = TableData[0].cust_dob
    let gender = TableData[0].cust_gender
        
        if (gender == "Male") {
        self.boyButton.setImage(UIImage(named: "Boy-50.png"), forState: UIControlState.Normal);
        }
        else if (gender == "Female") {
        self.girlButton.setImage(UIImage(named: "Girl-50.png"), forState: UIControlState.Normal);
        }
        
    }
    
    
    func updateAccount() {
        
        // Validations
        let password = self.passwordTextField.text!
        let repeatPassword = self.confirmPasswordTextField.text!
        
        if (usernameTextField.text!.isEmpty || passwordTextField.text!.isEmpty) {
            let alertController = UIAlertController(title: "Please enter your Username and Password", message:
                "", preferredStyle: UIAlertControllerStyle.Alert)
            alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.Default,handler: nil))
            
            self.activityIndicator.stopAnimating()
            self.activityIndicator.hidden = true
            
            self.presentViewController(alertController, animated: true, completion: nil)
        }
        else if (firstNameTextField.text!.isEmpty) {
            
            let alertController = UIAlertController(title: "Please enter your Firstname", message:
                "", preferredStyle: UIAlertControllerStyle.Alert)
            alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.Default,handler: nil))
            self.activityIndicator.stopAnimating()
            self.activityIndicator.hidden = true
            self.presentViewController(alertController, animated: true, completion: nil)
        }
        else if (lastNameTextField.text!.isEmpty) {
            
            let alertController = UIAlertController(title: "Please enter your Last Name", message:
                "", preferredStyle: UIAlertControllerStyle.Alert)
            alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.Default,handler: nil))
            self.activityIndicator.stopAnimating()
            self.activityIndicator.hidden = true
            self.presentViewController(alertController, animated: true, completion: nil)
        }
            
        else if (confirmPasswordTextField.text!.isEmpty ) {
            
            let alertController = UIAlertController(title: "Please enter your Password again", message:
                "", preferredStyle: UIAlertControllerStyle.Alert)
            alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.Default,handler: nil))
            self.activityIndicator.stopAnimating()
            self.activityIndicator.hidden = true
            self.presentViewController(alertController, animated: true, completion: nil)
            
        }
        else if (password != repeatPassword) {
            
            let alertController = UIAlertController(title: "Passord do not match", message:
                "", preferredStyle: UIAlertControllerStyle.Alert)
            alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.Default,handler: nil))
            self.activityIndicator.stopAnimating()
            self.activityIndicator.hidden = true
            self.presentViewController(alertController, animated: true, completion: nil)
            
        }
        else if (emailTextField.text!.isEmpty || emailTextField.text=="Enter your email") {
            
            let alertController = UIAlertController(title: "Please enter your Email Address", message:
                "", preferredStyle: UIAlertControllerStyle.Alert)
            alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.Default,handler: nil))
            self.activityIndicator.stopAnimating()
            self.activityIndicator.hidden = true
            self.presentViewController(alertController, animated: true, completion: nil)
        }
        else if (!isValidEmail(emailTextField.text!)) {
            
            let alertController = UIAlertController(title: "Please enter valid Email Address", message:
                "", preferredStyle: UIAlertControllerStyle.Alert)
            alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.Default,handler: nil))
            self.activityIndicator.stopAnimating()
            self.activityIndicator.hidden = true
            self.presentViewController(alertController, animated: true, completion: nil)
        }
            
        else {
      //      if Reachability.isConnectedToNetwork() == true {
            let status = checkNetworkStatus()
            
            if status == true {
                
                //    let selectedGender = gender.stringForKey("gender")
                let firstName = self.firstNameTextField.text
                let lastName = self.lastNameTextField.text
                let userName = self.usernameTextField.text
                let password = self.passwordTextField.text
                let phone = self.phoneNumberTextField.text
                let email = self.emailTextField.text
                let address = self.addressTextField.text
                let city = self.cityTextField.text
                let state = self.stateTextField.text
                let dateOfBirth = self.DateOfBirthTextField.text
                
                
                
                let cust_fb_reg_id = "dsadq34343423"
                let apiURL = api_url+"create_user.php"
                
                let request = NSMutableURLRequest(URL: NSURL(string: apiURL)!)
                request.HTTPMethod = "POST"
                let postString = "cust_firstname=\(firstName!)&cust_lastname=\(lastName!)&cust_username=\(userName!)&cust_password=\(password!)&cust_phone=\(phone!)&cust_email=\(email!)&cust_address=\(address!)&cust_city=\(city!)&cust_state=\(state!)&cust_dob=\(dateOfBirth!)&cust_reg_id=\(cust_fb_reg_id)&apiKey=\(apiKey)"
                request.HTTPBody = postString.dataUsingEncoding(NSUTF8StringEncoding)
                let configuration = NSURLSessionConfiguration.defaultSessionConfiguration() // added for fix
                let session = NSURLSession(configuration: configuration, delegate:nil, delegateQueue:NSOperationQueue.mainQueue()) // added for fix
                let task = session.dataTaskWithRequest(request) {
                    data, response, error in
                    
                    if error != nil {
                        print("Errrrrrrr.......... error=\(error)")
                        return
                    }
                    
                    print("responseString = \(response)")
                    
                    let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)!
                    print("responseString_username = \(responseString)")
                    
                    
                    do {
                        let json = try NSJSONSerialization.JSONObjectWithData(data!, options: .AllowFragments)
                        
                        let response = json["response"] as! String
                        let cust_id = json["cust_id"] as! String
                        
                        print ("Final Response: \(response)")
                        
                        if (response == "USER_CREATED_SUCCESSFULLY")    {
                            self.activityIndicator.stopAnimating()
                            self.activityIndicator.hidden = true
                            print("USER_CREATED_SUCCESSFULLY")
                            
                            self.loginUsername.setObject("\(userName)", forKey: "loginUsername")
                            self.loginPassword.setObject("\(password)", forKey: "loginPassword")
                            self.loginCustID.setObject("\(cust_id)", forKey: "loginCustID")
                            
                            // alert box code below
                            let alert = UIAlertController(title: "Success!", message:"User account created successfully.", preferredStyle: .Alert)
                            let action = UIAlertAction(title: "OK", style: .Default) { _ in
                                // Put here any code that you would like to execute when
                                // the user taps that OK button (may be empty in your case if that's just
                                // an informative alert
                                if let pageViewController =
                                    self.storyboard?.instantiateViewControllerWithIdentifier("WalkthroughController")
                                        as? WalkthroughPageViewController {
                                    self.presentViewController(pageViewController, animated: true, completion:
                                        nil)
                                }
                            }
                            alert.addAction(action)
                            self.presentViewController(alert, animated: true, completion: nil)
                            // alert box code end
                            
                        }
                        else {
                            self.activityIndicator.stopAnimating()
                            self.activityIndicator.hidden = true
                            print("USERNAME_ALREADY_EXISTS")
                            //                        let alert = UIAlertView(title: "Error", message: "Invalid current Password or password do not match. Please enter again.", delegate: nil, cancelButtonTitle: "OK")
                            //                        alert.show()
                            // alert box code below
                            let alert = UIAlertController(title: "Error", message:"Username already exists. Please try with a different user.", preferredStyle: .Alert)
                            let action = UIAlertAction(title: "OK", style: .Default) { _ in
                                // Put here any code that you would like to execute when
                                // the user taps that OK button (may be empty in your case if that's just
                                // an informative alert
                            }
                            alert.addAction(action)
                            self.presentViewController(alert, animated: true, completion: nil)
                            // alert box code end
                            
                        }
                        
                        
                        
                    } catch {
                        print("error serializing JSON: \(error)")
                    }
                    
                    
                    
                }
                task.resume()
                
                
                
            }
            else {
                // alert box code below
                let alert = UIAlertController(title: "Not Connected", message:"You are not connected to internet!.", preferredStyle: .Alert)
                let action = UIAlertAction(title: "OK", style: .Default) { _ in
                    // Put here any code that you would like to execute when
                    // the user taps that OK button (may be empty in your case if that's just
                    // an informative alert
                    self.dismissViewControllerAnimated(true, completion: {})
                }
                alert.addAction(action)
                self.presentViewController(alert, animated: true, completion: nil)
                // alert box code end
            }
        }
    }
    
    
    func isValidEmail(testStr:String) -> Bool {
        // println("validate calendar: \(testStr)")
        let emailRegEx = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluateWithObject(testStr)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
